/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_3.pkg2;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Xavier
 */
public class Automovel implements Comparable<Automovel>, Serializable {

    private String marca;
    private String modelo;
    private int ano;
    private String matricula;
    private int kms;

    public Automovel() {

    }

    public Automovel(String marca, String modelo, int ano, String matricula, int kms) {
        this.marca = marca;
        this.modelo = modelo;
        this.ano = ano;
        this.matricula = matricula;
        this.kms = kms;
    }

 

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    
    

    public int getKms() {
        return kms;
    }

    public void setKms(int kms) {
        this.kms = kms;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Automovel other = (Automovel) obj;
        if (!Objects.equals(this.matricula, other.matricula)) {
            return false;
        }
        return true;
    }



    @Override
    public String toString() {
        return "Automovel{" + "Marca: " + marca + ", Modelo: " + modelo + ", Ano: " + ano + ", Matricula: "  + matricula + ", Kms: " + kms + '}';
    }

 

  

    @Override
    public int compareTo(Automovel o) {
        if (this.kms > o.getKms()) {
            return 1;
        }
        if (this.kms < o.getKms()) {
            return -1;
        }
        return 0;
    }

}
