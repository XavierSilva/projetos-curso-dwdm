/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_3.pkg2;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author Xavier
 */
public class Armazenar {

    public int salvarDados(Listas listas) {
        try {

            FileOutputStream fs = new FileOutputStream("listas.arq"); //tanto faz o nome do arquivo
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(listas); //referencia a estrutura que se quer armazenar
            os.close();
            //System.out.println("Dados Salvos com Sucesso!");
            return 0;
        } catch (Exception ex) {
            return -1;

        }
    }

    public Listas lerDados(String fich) {
        try {
            DoubleOrderedList listFrota = new DoubleOrderedList();
            DoubleOrderedList listALugados = new DoubleOrderedList();

            Listas listas = new Listas(listFrota, listALugados);

            FileInputStream fs = new FileInputStream(fich); //tanto faz o nome do arquivo
            ObjectInputStream os = new ObjectInputStream(fs);

            listas = (Listas) os.readObject(); //referencia a estrutura que se quer armazenar
            os.close();
            System.out.println("Dados Carregados com Sucesso!");
            return listas;
        } catch (Exception ex) {
            return null;

        }
    }

}
