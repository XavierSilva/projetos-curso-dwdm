/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_3.pkg2;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author Xavier
 */
public class Projeto_32 implements Serializable{

    /**
     * @param args the command line arguments
     */
    static DoubleOrderedList<Automovel> listFrota = new DoubleOrderedList<Automovel>();
    static DoubleOrderedList<Automovel> listALugados = new DoubleOrderedList<Automovel>();

    public static void main(String[] args) {

        Listas listas = new Listas();
        listas = ler();
        if (listas == null) {
            System.out.println("Não foi possivel ler");
            listas = new Listas();
        }
        menu(listas);
    }

    static void mostrarFrotas(Listas listas) {
        Scanner captura = new Scanner(System.in);
        int pedido;
        do {
            System.out.println("");
            System.out.println("");
            System.out.println("========== Frota  ==========");
            System.out.println("  Escolha uma opção: ");
            System.out.println("1 - Mostrar Frota Disponivel");
            System.out.println("2 - Mostrar Frota Alugada");
            System.out.println("0 - Voltar");
            System.out.println("Selecione um número: ");
            pedido = captura.nextInt();
            switch (pedido) {

                case 1:
                    listas.mostrarFrota();
                    break;
                case 2:
                    listas.mostrarFrotaAlugada();
                    break;

                case 0:
                    break;
                default:
                    System.out.println("Erro! Número não existe no menu!");
                    break;
            }
        } while (pedido != 0);

    }

    public static Listas ler() {

        Listas listas = new Listas();

        Armazenar grava = new Armazenar();
        listas = grava.lerDados("listas.arq");
        return listas;
    }

    //grava informação da agenda num ficheiro
    public static void gravar(Listas listas) {

        Armazenar grava = new Armazenar();

        grava.salvarDados(listas);

    }

    private static void menu(Listas listas) {
        Scanner captura = new Scanner(System.in);
        int pedido;
        do {
            System.out.println("");
            System.out.println("");
            System.out.println("========== Menu  ==========");
            System.out.println("  Escolha uma opção: ");
            System.out.println("1 - Adicionar Automóvel ");
            System.out.println("2 - Remover Automóvel");
            System.out.println("3 - Editar Automóvel");
            System.out.println("4 - Mostrar Frota");
            System.out.println("5 - Alugar Automóvel");
            System.out.println("6 - Devolver Automóvel");
            System.out.println("0 - Sair");
            System.out.println("Selecione um número: ");
            pedido = captura.nextInt();
            switch (pedido) {
                case 1:
                    listas.adicionarAutomovel();
                    gravar(listas);
                    break;
                case 2:
                    listas.removerAutomovel();
                    gravar(listas);
                    break;
                case 3:
                    listas.editarAutomovel();
                    gravar(listas);
                    break;
                case 4:
                    mostrarFrotas(listas);
                    break;
                case 5:
                    listas.alugarAutomovel();
                    gravar(listas);
                    break;
                case 6:
                    listas.devolverAutomovel();
                    gravar(listas);
                    break;
                case 0:
                    System.out.println("Dados Guardados! Obrigado!");
                    break;
                default:
                    System.out.println("Erro! Número não existe no menu!");
                    break;
            }
        } while (pedido != 0);
    }

}
