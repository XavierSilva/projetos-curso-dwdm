/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_3.pkg2;

/**
 *
 * @author rjs
 */
public interface OrderedListADT<T> extends ListADT<T> {

    /**
     * Adds the specified element to this list at the proper location
     *
     * @param element the element to be added to this list
     */
    public void add(T element) throws NonComparableElementException;
}