/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_3.pkg2;

/**
 *
 * @author rjs
 */
public class ElementNotFoundException extends RuntimeException
{
   /**
    * Sets up this exception with an appropriate message.
    */
   public ElementNotFoundException (String collection)
   {
      super ("The target element is not in this " + collection);
   }
}
