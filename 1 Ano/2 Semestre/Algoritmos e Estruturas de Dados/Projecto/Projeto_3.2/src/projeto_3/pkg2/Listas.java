/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_3.pkg2;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author Xavier
 */
public class Listas implements Serializable {

    private DoubleOrderedList<Automovel> listFrota = new DoubleOrderedList();
    private DoubleOrderedList<Automovel> listALugados = new DoubleOrderedList();

    public Listas() {
    }

    public Listas(DoubleOrderedList<Automovel> listFrota, DoubleOrderedList<Automovel> listALugados) {
        this.listFrota = listFrota;
        this.listALugados = listALugados;
    }

    public void adicionarAutomovel() {

        System.out.println("Insira os dados:");
        Scanner captura = new Scanner(System.in);
        Automovel a = new Automovel();
        System.out.println("Marca:");
        a.setMarca(captura.nextLine());
        System.out.println("Modelo:");
        a.setModelo(captura.nextLine());
        System.out.println("Ano:");
        a.setAno(captura.nextInt());
        System.out.println("Matricula:");
        a.setMatricula(captura.next());
        System.out.println("Kilometros:");
        a.setKms(captura.nextInt());
        if (!listFrota.contains(a)) {
            try {
                listFrota.add(a);
                System.out.println("Automovel adicionado com sucesso!");
            } catch (Exception e) {
            }
        } else {
            System.out.println("ERRO!  Já existe na frota esse automóvel! / Matricula encontrada!");
        }

    }

    public void removerAutomovel() {
        System.out.println("Insira os dados:");
        Scanner captura = new Scanner(System.in);
        Automovel a = new Automovel();
        System.out.println("Matricula:");
        a.setMatricula(captura.next());
        if (listFrota.contains(a)) {
            listFrota.remove(a);
            System.out.println("Automovel removido da frota!");
        } else {
            System.out.println("ERRO!  Não existe na frota esse automóvel!");
        }

    }

    public void editarAutomovel() {
        System.out.println("Selecione o Automovel:");
        Scanner captura = new Scanner(System.in);
        Automovel a = new Automovel();
        System.out.println("Matricula:");
        a.setMatricula(captura.next());
        if (listFrota.contains(a)) {
            listFrota.remove(a);

            adicionarAuto(a);
        } else {
            System.out.println("ERRO!  Não existe na frota esse automóvel!");
        }

    }

    public void mostrarFrota() {

        if (listFrota.isEmpty()) {
            System.out.println("Não existem automoveis!!");

        } else {

            Iterator itr = listFrota.iterator();
            while (itr.hasNext()) {
                System.out.println(itr.next());
            }
            System.out.println("Total de Automoveis: " + listFrota.count);
        }
    }

    public void mostrarFrotaAlugada() {

        if (listALugados.isEmpty()) {
            System.out.println("Não tem automoveis alugados de momento!");

        } else {
            Iterator itr = listALugados.iterator();
            while (itr.hasNext()) {
                System.out.println(itr.next());
            }
            System.out.println("Total de Automoveis: " + listALugados.count);
        }
    }

    public void adicionarAuto(Automovel a) {
        System.out.println("Redefina os dados:");
        Scanner captura = new Scanner(System.in);
        System.out.println("Marca:");
        a.setMarca(captura.nextLine());
        System.out.println("Modelo:");
        a.setModelo(captura.nextLine());
        System.out.println("Ano:");
        a.setAno(captura.nextInt());
        System.out.println("Kilometros:");
        a.setKms(captura.nextInt());
        if (!listFrota.contains(a)) {
            try {
                listFrota.add(a);
                System.out.println("Dados actualizados com sucesso");
            } catch (Exception e) {
            }
        } else {
            System.out.println("ERRO!  Já existe na frota esse automóvel! / Matricula encontrada!");
        }
    }

    public void alugarAutomovel() {

        Automovel a = new Automovel();

        if (!listFrota.isEmpty()) {
            a = listFrota.removeFirst();
            try {
                listALugados.add(a);
                System.out.println("Foi-lhe atribuido o Automovel: " + a.toString());
            } catch (Exception e) {
            }

        } else {
            System.out.println("Não existem automoveis para alugar!");
        }
    }

    public void devolverAutomovel() {
        System.out.println("Automovel a Devolver: ");
        Scanner captura = new Scanner(System.in);
        Automovel a = new Automovel();
        System.out.println("Matricula:");
        a.setMatricula(captura.next());
        int kmsNovos = -1;
        if (!listALugados.isEmpty()) {
            if (listALugados.contains(a)) {
                a = listALugados.remove(a);
                do {
                    if (kmsNovos != -1) {
                        System.out.println("Kilometros inferiores ao registo anterior!");
                    }
                    System.out.println("Insira Kilometros atuais:");
                    kmsNovos = (captura.nextInt());
                } while (kmsNovos < a.getKms());
                a.setKms(kmsNovos);
                try {
                    listFrota.add(a);
                    System.out.println("Automovel Devolvido! :  " + a.toString());
                } catch (Exception e) {
                }
            } else {
                System.out.println("ERRO -> O Automovel indicado ainda esta alugado!");
            }

        } else {
            System.out.println("Erro -> Não existem veículos alugados!");
        }

    }

    public void adicionarAutomovelTeste() {

        for (int i = 0; i <= 140; i++) {
            Automovel a = new Automovel();
            a.setMarca(String.valueOf(i));
            a.setMatricula("a" + i);
            a.setKms(i);
            if (!listFrota.contains(a)) {
                try {
                    listFrota.add(a);
                    System.out.println("Automovel adicionado com sucesso!");
                } catch (Exception e) {
                }
            } else {
                System.out.println("ERRO!  Já existe na frota esse automóvel! / Matricula encontrada!");
            }
        }

    }

}
