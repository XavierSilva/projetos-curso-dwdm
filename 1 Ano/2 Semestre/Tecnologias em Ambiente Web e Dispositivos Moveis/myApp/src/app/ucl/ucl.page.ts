import { Component, OnInit } from '@angular/core';
import { UclserviceService } from './uclservice.service';

@Component({
  selector: 'ucl',
  templateUrl: './ucl.page.html',
  styleUrls: ['./ucl.page.scss'],
})
export class UclPage implements OnInit {

  equipas:{};

  constructor(private uclservice: UclserviceService) { }

  ngOnInit() { 
      this.uclservice.getEquipas().subscribe(equipas => this.equipas = equipas)  
  }

}
