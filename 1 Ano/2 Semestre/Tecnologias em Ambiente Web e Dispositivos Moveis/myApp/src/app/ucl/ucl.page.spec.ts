import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UclPage } from './ucl.page';

describe('UclPage', () => {
  let component: UclPage;
  let fixture: ComponentFixture<UclPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UclPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UclPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
