import { Component } from '@angular/core';
import { HomeserviceService } from './homeservice.service';
import { Coment } from './coment';
import { COMENT } from './mocks';

@Component({
  selector: 'home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  pl: {};
  cl: {};
  comentarios: Coment[];
  com:string;
  
  constructor(private homeservice: HomeserviceService) {    
      
    
    
   }
  ngOnInit() {
    this.homeservice.getInfoUCL().subscribe(cl => this.cl = cl)
    this.homeservice.getInfoPPl().subscribe(pl => this.pl = pl)
    this.comentarios = COMENT;    
  }
  addc(){    
    let c = new Coment();
    c.autor="User";    
    c.comentario= this.com;
    this.comentarios.push(c);
    this.com="";    
  }
} 
