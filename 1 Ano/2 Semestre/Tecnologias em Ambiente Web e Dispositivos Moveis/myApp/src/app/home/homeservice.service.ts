import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
//import { EquipasPPl } from './EquipasPpl';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class HomeserviceService {

  apiKey = "8fe6c9beaacc4c4d9709a865ae3cf5c8";
  constructor(private http: Http) {
  }

  getInfoPPl() {
    let headers = new Headers();
    let apiUrl = 'https://api.football-data.org/v2/competitions/PPL';
    headers.append('X-Auth-Token', '8fe6c9beaacc4c4d9709a865ae3cf5c8');
    let options = new RequestOptions({ headers: headers });
    return this.http.get(apiUrl, options).map(response => response.json());
    
  }
  getInfoUCL() {
    let headers = new Headers();
    let apiUrl = 'https://api.football-data.org/v2/competitions/CL';
    headers.append('X-Auth-Token', '8fe6c9beaacc4c4d9709a865ae3cf5c8');
    let options = new RequestOptions({ headers: headers });
    return this.http.get(apiUrl, options).map(response => response.json());
    
  }
}
