import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Http, HttpModule } from '@angular/http';
import { HomeserviceService } from './home/homeservice.service';
import { PplserviceService } from './ppl/pplservice.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UclserviceService } from './ucl/uclservice.service';
import { TeamserviceService } from './team/teamservice.service';



@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpModule,
    NgbModule.forRoot(),
    

  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    HttpModule,
    HomeserviceService,
    PplserviceService,
    UclserviceService,
    TeamserviceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
