import { Image } from './Image';

export const IMAGENS: Image[] = [
    {
        "id": 15898,
        "imgUrl": "assets/planteis/porto/casilhas.png"
    },
    {
        "id": 15900,
        "imgUrl": "assets/planteis/porto/fabiano.png"
    },
    {
        "id": 15901,
        "imgUrl": "assets/planteis/porto/vana.png"
    },
    {
        "id": 1337,
        "imgUrl": "assets/planteis/porto/militao.png"
    },
    {
        "id": 3163,
        "imgUrl": "assets/planteis/porto/maxi.png"
    },
    {
        "id": 7918,
        "imgUrl": "assets/planteis/porto/mbemba.png"
    },
    {
        "id": 10181,
        "imgUrl": "assets/planteis/porto/pepe.png"
    },
    {
        "id": 15903,
        "imgUrl": "assets/planteis/porto/felipe.png"
    }, {
        "id": 15904,
        "imgUrl": "assets/planteis/porto/telles.png"
    },
    {
        "id": 37377,
        "imgUrl": "assets/planteis/porto/queiros.png"
    },
    {
        "id": 37796,
        "imgUrl": "assets/planteis/porto/leite.png"
    },
    {
        "id": 45362,
        "imgUrl": "assets/planteis/porto/manafa.png"
    }, 
    {
        "id": 1387,
        "imgUrl": "assets/planteis/porto/jp.png"
    },
    {
        "id": 3294,
        "imgUrl": "assets/planteis/porto/herrera.png"
    },
    {
        "id": 3296,
        "imgUrl": "assets/planteis/porto/corona.png"
    },
    {
        "id": 9428,
        "imgUrl": "assets/planteis/porto/bazoer.png"
    }, 
    {
        "id": 15907,
        "imgUrl": "assets/planteis/porto/brahimi.png"
    },
    {
        "id": 15909,
        "imgUrl": "assets/planteis/porto/danilo.png"
    },
    {
        "id": 15910,
        "imgUrl": "assets/planteis/porto/otavio.png"
    },
    {
        "id": 15911,
        "imgUrl": "assets/planteis/porto/oliver.png"
    }, 
    {
        "id": 37331,
        "imgUrl": "assets/planteis/porto/mamadou.png"
    },
    {
        "id": 94,
        "imgUrl": "assets/planteis/porto/adrian.png"
    },
    {
        "id": 15914,
        "imgUrl": "assets/planteis/porto/soares.png"
    },
    {
        "id": 15915,
        "imgUrl": "assets/planteis/porto/aboubakar.png"
    },
    {
        "id": 15916,
        "imgUrl": "assets/planteis/porto/hernani.png"
    },
    {
        "id": 15917,
        "imgUrl": "assets/planteis/porto/marega.png"
    },
    {
        "id": 15920,
        "imgUrl": "assets/planteis/porto/bc.png"
    },
    {
        "id": 37357,
        "imgUrl": "assets/planteis/porto/pereira.png"
    },
    {
        "id": 37400,
        "imgUrl": "assets/planteis/porto/fernando.png"
    },
    {
        "id": 76174,
        "imgUrl": "assets/planteis/porto/marius.png"
    },
    {
        "id": 15922,
        "imgUrl": "assets/planteis/porto/sc.png"
    },
    {
        "id": 37362,
        "imgUrl": "assets/planteis/porto/dc.png"
    },
    {
        "id": 51,
        "imgUrl": "assets/planteis/real/navas.jpg"
    },
    {
        "id": 3641,
        "imgUrl": "assets/planteis/real/courtois.png"
    },
    {
        "id": 3871,
        "imgUrl": "assets/planteis/real/luca.jpg"
    },
    {
        "id": 46,
        "imgUrl": "assets/planteis/real/vallejo.jpg "
    },
    {
        "id": 48,
        "imgUrl": "assets/planteis/real/marcelo.jpg"
    },
    {
        "id": 50,
        "imgUrl": "assets/planteis/real/nacho.jpg"
    },
    {
        "id": 3192,
        "imgUrl": "assets/planteis/real/ramos.jpg"
    },
    {
        "id": 3194,
        "imgUrl": "assets/planteis/real/carvajal.jpg "
    },
    {
        "id": 3360,
        "imgUrl": "assets/planteis/real/varane.jpg"
    },
    {
        "id": 51116,
        "imgUrl": "assets/planteis/real/reguilon.jpg"
    },
    {
        "id": 80899,
        "imgUrl": "assets/planteis/real/javier.jpg"
    },
    {
        "id": 98741,
        "imgUrl": "assets/planteis/real/adrian.jpg "
    },
    {
        "id": 43,
        "imgUrl": "assets/planteis/real/modric.jpg"
    },
    {
        "id": 45,
        "imgUrl": "assets/planteis/real/lucas.jpg"
    },
    {
        "id": 47,
        "imgUrl": "assets/planteis/real/kross.jpg"
    },
    {
        "id": 52,
        "imgUrl": "assets/planteis/real/asensio.jpg "
    },
    {
        "id": 66,
        "imgUrl": "assets/planteis/real/isco.jpg"
    },
    {
        "id": 68,
        "imgUrl": "assets/planteis/real/dani.jpg"
    },
    {
        "id": 70,
        "imgUrl": "assets/planteis/real/marcos.jpg"
    },
    {
        "id": 96,
        "imgUrl": "assets/planteis/real/valverde.jpg "
    },
    {
        "id": 3231,
        "imgUrl": "assets/planteis/real/casemiro.jpg"
    },
    {
        "id": 3876,
        "imgUrl": "assets/planteis/real/bale.jpg"
    },
    {
        "id": 3878,
        "imgUrl": "assets/planteis/real/jaime.jpg"
    },
    {
        "id": 7887,
        "imgUrl": "assets/planteis/real/diaz.jpg "
    },
    {
        "id": 99992,
        "imgUrl": "assets/planteis/real/fidalgo.jpg"
    },
    {
        "id": 102250,
        "imgUrl": "assets/planteis/real/grau.jpg"
    },
    {
        "id": 11,
        "imgUrl": "assets/planteis/real/diaz.jpg"
    },
    {
        "id": 49,
        "imgUrl": "assets/planteis/real/benzema.jpg "
    },
    {
        "id": 1556,
        "imgUrl": "assets/planteis/real/vinicius.jpg "
    },
    {
        "id": 3879,
        "imgUrl": "assets/planteis/real/abalo.jpg"
    },
    {
        "id": 3880,
        "imgUrl": "assets/planteis/real/cristo.jpg"
    },
    {
        "id": 8474,
        "imgUrl": "assets/planteis/real/mariano.jpg"
    },
    {
        "id": 99993,
        "imgUrl": "assets/planteis/real/garcia.jpg "
    },
    {
        "id": 79,
        "imgUrl": "assets/planteis/real/zidane.jpg "
    },
    {
        "id": 15875,
        "imgUrl": "assets/planteis/sporting/salin.png "
    },
    {
        "id": 8713,
        "imgUrl": "assets/planteis/aves/berna.jpg "
    },
    {
        "id": 45246,
        "imgUrl": "assets/planteis/aves/marco.jpg "
    },
    {
        "id": 52791,
        "imgUrl": "assets/planteis/aves/andre.jpg "
    },
    {
        "id": 90834,
        "imgUrl": "assets/planteis/aves/fabio.jpg "
    },
    {
        "id": 111747,
        "imgUrl": "assets/planteis/aves/fabio.jpg "
    },
    {
        "id": 23148,
        "imgUrl": "assets/planteis/aves/mato.jpg "
    },
    {
        "id": 37234,
        "imgUrl": "assets/planteis/aves/costa.jpg "
    },
    {
        "id": 43698,
        "imgUrl": "assets/planteis/aves/defendi.jpg "
    },
    {
        "id": 43702,
        "imgUrl": "assets/planteis/aves/galo.jpg "
    },
    {
        "id": 43712,
        "imgUrl": "assets/planteis/aves/felipe.jpg "
    },
    {
        "id": 45248,
        "imgUrl": "assets/planteis/aves/rodrigo.jpg "
    },
    {
        "id": 53190,
        "imgUrl": "assets/planteis/aves/mangas.jpg "
    },
    {
        "id": 1222,
        "imgUrl": "assets/planteis/aves/marcelo.jpg "
    },
    {
        "id": 12773,
        "imgUrl": "assets/planteis/aves/erik.jpg "
    },
    {
        "id": 37456,
        "imgUrl": "assets/planteis/aves/oliveira.jpg "
    },
    {
        "id": 43696,
        "imgUrl": "assets/planteis/aves/gomes.jpg "
    },
    {
        "id": 43699,
        "imgUrl": "assets/planteis/aves/ponk.jpg "
    },
    {
        "id": 43703,
        "imgUrl": "assets/planteis/aves/braga.jpg "
    },
    {
        "id": 43706,
        "imgUrl": "assets/planteis/aves/farina.jpg "
    },
    {
        "id": 43707,
        "imgUrl": "assets/planteis/aves/balde.jpg "
    },
    {
        "id": 43711,
        "imgUrl": "assets/planteis/aves/falcao.jpg "
    },
    {
        "id": 76843,
        "imgUrl": "assets/planteis/aves/bura.jpg "
    },
    {
        "id": 79820,
        "imgUrl": "assets/planteis/aves/defendi.jpg "
    },
    {
        "id": 113219,
        "imgUrl": "assets/planteis/aves/faye.jpg "
    },
    {
        "id": 115038,
        "imgUrl": "assets/planteis/aves/varela.jpg "
    },
    {
        "id": 12783,
        "imgUrl": "assets/planteis/aves/caique.jpg "
    },
    {
        "id": 37498,
        "imgUrl": "assets/planteis/aves/abdou.jpg "
    },
    {
        "id": 43709,
        "imgUrl": "assets/planteis/aves/derley.jpg "
    },
    {
        "id": 45186,
        "imgUrl": "assets/planteis/aves/gomes.jpg "
    },
    {
        "id": 53444,
        "imgUrl": "assets/planteis/aves/defendi.jpg "
    },
    {
        "id": 97480,
        "imgUrl": "assets/planteis/aves/rodrigues.jpg "
    },
    {
        "id": 102224,
        "imgUrl": "assets/planteis/aves/luquinhas.jpg "
    },
    {
        "id": 111748,
        "imgUrl": "assets/planteis/aves/tavares.jpg "
    },
    {
        "id": 102352,
        "imgUrl": "assets/planteis/aves/inacio.jpg "
    },
    {
        "id": 3355,
        "imgUrl": "assets/planteis/tot/hugo.png "
    },
    {
        "id": 7991,
        "imgUrl": "assets/planteis/tot/vorm.png "
    },
    {
        "id": 7992,
        "imgUrl": "assets/planteis/tot/alfie.png "
    },
    {
        "id": 7993,
        "imgUrl": "assets/planteis/tot/paulo.png "
    },
    {
        "id": 3312,
        "imgUrl": "assets/planteis/tot/kieran.png "
    },
    {
        "id": 3312,
        "imgUrl": "assets/planteis/tot/kieran.png "
    },
    {
        "id": 3314,
        "imgUrl": "assets/planteis/tot/eric.png "
    },
    {
        "id": 3646,
        "imgUrl": "assets/planteis/tot/jan.png "
    },
    {
        "id": 3648,
        "imgUrl": "assets/planteis/tot/toby.png "
    },
    {
        "id": 6188,
        "imgUrl": "assets/planteis/tot/luke.png "
    },
    {
        "id": 7994,
        "imgUrl": "assets/planteis/tot/ben.png "
    },
    {
        "id": 7995,
        "imgUrl": "assets/planteis/tot/kyle.png "
    },
    {
        "id": 7996,
        "imgUrl": "assets/planteis/tot/juan.png "
    },
    {
        "id": 7997,
        "imgUrl": "assets/planteis/tot/serge.png "
    },
    {
        "id": 98742,
        "imgUrl": "assets/planteis/tot/timo.png "
    },
    {
        "id": 3319,
        "imgUrl": "assets/planteis/tot/danny.png "
    },
    {
        "id": 3324,
        "imgUrl": "assets/planteis/tot/dele.png "
    },
    {
        "id": 3343,
        "imgUrl": "assets/planteis/tot/son.png "
    },
    {
        "id": 3459,
        "imgUrl": "assets/planteis/tot/eriksen.png "
    },
    {
        "id": 3733,
        "imgUrl": "assets/planteis/tot/davinson.png "
    },
    {
        "id": 7999,
        "imgUrl": "assets/planteis/tot/winks.png "
    },
    {
        "id": 8000,
        "imgUrl": "assets/planteis/tot/victor.png "
    },
    {
        "id": 8001,
        "imgUrl": "assets/planteis/tot/moussa.png "
    },
    {
        "id": 8003,
        "imgUrl": "assets/planteis/tot/lucas.png "
    },
    {
        "id": 96974,
        "imgUrl": "assets/planteis/tot/oliver.png "
    },
    {
        "id": 98743,
        "imgUrl": "assets/planteis/tot/marsh.png "
    },
    {
        "id": 8004,
        "imgUrl": "assets/planteis/tot/harry.png "
    },
    {
        "id": 8005,
        "imgUrl": "assets/planteis/tot/erik.png "
    },
    {
        "id": 8007,
        "imgUrl": "assets/planteis/tot/lorent.png "
    },
    {
        "id": 11607,
        "imgUrl": "assets/planteis/tot/vincent.png "
    },
    {
        "id": 11609,
        "imgUrl": "assets/planteis/tot/poche.png "
    }

]