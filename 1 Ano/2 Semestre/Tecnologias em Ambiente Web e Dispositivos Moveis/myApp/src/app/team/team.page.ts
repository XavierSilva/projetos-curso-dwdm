import { Component, OnInit } from '@angular/core';
import { TeamserviceService } from './teamservice.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { initDomAdapter } from '@angular/platform-browser/src/browser';
import { IMAGENS } from './mocks';
import { Image } from './Image';

@Component({
  selector: 'team',
  templateUrl: './team.page.html',
  styleUrls: ['./team.page.scss'],
})
export class TeamPage implements OnInit {

  plantel: {};
  teamID: String = "";
  imagens: Image[];

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private teamservice: TeamserviceService) { }

  ngOnInit() {
    this.imagens = IMAGENS;
    this.teamID = null;
    this.initId();
  }
  getId() {
    this.teamservice.getPlantel(this.teamID).subscribe(plantel => this.plantel = this.plantel)
  }
  initId() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id != null) {
      this.teamID = id;
    }
    this.teamservice.getPlantel(this.teamID).subscribe(plantel => this.plantel = plantel);
  }
  getImagem(id) {
    for (let e of this.imagens) {
      if (e.id == id) {
        return e.imgUrl;
      }
    }
  }
  getIdade(date) {
    let dat = new Date();
    let res = date.split("-");
    
    if (date[1] < dat.getMonth()) {
      if(date[2] < dat.getDay()){
        return dat.getFullYear() - res[0] + 1;
      }
    } else {
      return dat.getFullYear() - res[0];
    }
  }

}
