import { Injectable } from '@angular/core';
import { Http, RequestOptions,Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class TeamserviceService {

  apiKey = "8fe6c9beaacc4c4d9709a865ae3cf5c8";

  constructor(private http: Http) { 
  }  

  getPlantel2(id:String)  {
    let headers= new Headers();
    let apiUrl = 'https://api.football-data.org/v2/teams/2';
    apiUrl += id;
    headers.append('X-Auth-Token','8fe6c9beaacc4c4d9709a865ae3cf5c8');
    let options = new RequestOptions({headers:headers});    
    return this.http.get(apiUrl,options).map(response => response.json());
    //return this.http.get('https://api.football-data.org/v2/competitions/PPL?X-Auth-Token=8fe6c9beaacc4c4d9709a865ae3cf5c8').map(response => <EquipasPPl[]>response.json().data);
  }
  getPlantel(id:String)  {
    let headers= new Headers();
    let apiUrl = 'https://api.football-data.org/v2/teams/';
    apiUrl += id;
    headers.append('X-Auth-Token','8fe6c9beaacc4c4d9709a865ae3cf5c8');
    let options = new RequestOptions({headers:headers});    
    return this.http.get(apiUrl,options).map(response => response.json());
    //return this.http.get('https://api.football-data.org/v2/competitions/PPL?X-Auth-Token=8fe6c9beaacc4c4d9709a865ae3cf5c8').map(response => <EquipasPPl[]>response.json().data);
  }
}
