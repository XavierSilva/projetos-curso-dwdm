import { Image } from './Image';

export const IMAGENS: Image[] = [
    {
        "id": 496,
        "imgUrl": "assets/ppl/rioave.png"
    },
    {
        "id": 5531,
        "imgUrl": "assets/ppl/famalicao.png"
    },
    {
        "id": 5533,
        "imgUrl": "assets/ppl/gil.png"
    },
    {
        "id": 507,
        "imgUrl": "assets/ppl/pacos.png"
    },
    {
        "id": 498, 
        "imgUrl": "assets/ppl/sporting.png"
    },
    {
        "id": 503,
        "imgUrl": "assets/ppl/porto.png"
    },
    {
        "id": 583,
        "imgUrl": "assets/ppl/moreira.png"
    },
    {
        "id": 810,
        "imgUrl": "assets/ppl/boavista.png"
    },
    {
        "id": 1103,
        "imgUrl": "assets/ppl/chaves.png"
    },
    {
        "id": 1903,
        "imgUrl": "assets/ppl/benfica.png"
    },
    {
        "id": 5529,
        "imgUrl": "assets/ppl/nacional.png"   
    },
    {
        "id": 5530,
        "imgUrl": "assets/ppl/stclara.png"
    },
    {
        "id": 5543,
        "imgUrl": "assets/ppl/vsc.png"
    },
    {
        "id": 5544,
        "imgUrl": "assets/ppl/aves.png"
    },
    {
        "id": 5565,
        "imgUrl": "assets/ppl/feirense.png"
    },
    {
        "id": 5568,
        "imgUrl": "assets/ppl/belenenses.png"
    },
    {
        "id": 5575,
        "imgUrl": "assets/ppl/maritimo.png"
    },
    {
        "id": 5601,
        "imgUrl": "assets/ppl/portimonense.png"
    },
    {
        "id": 5613,
        "imgUrl": "assets/ppl/braga.png"
    },
    {
        "id": 5620,
        "imgUrl": "assets/ppl/vfc.png"
    },
    {
        "id": 1049,
        "imgUrl": "assets/ppl/tondela.png"
    }
]