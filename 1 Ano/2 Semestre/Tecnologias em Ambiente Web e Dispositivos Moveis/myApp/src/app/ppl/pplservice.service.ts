import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers} from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class PplserviceService {

  apiKey = "8fe6c9beaacc4c4d9709a865ae3cf5c8";
  private apiUrl='https://api.football-data.org/v2/competitions/PPL/teams';

  constructor(private http: Http) {     
  }  

  getEquipas()  {
    let headers= new Headers();
    headers.append('X-Auth-Token','8fe6c9beaacc4c4d9709a865ae3cf5c8');
    let options = new RequestOptions({headers:headers});    
    return this.http.get(this.apiUrl ,options).map(response => response.json());
    //return this.http.get('https://api.football-data.org/v2/competitions/PPL?X-Auth-Token=8fe6c9beaacc4c4d9709a865ae3cf5c8').map(response => <EquipasPPl[]>response.json().data);
  }
}
