import { Component, OnInit } from '@angular/core';
import {IMAGENS} from './mocks';
import { Image } from './Image';
import { PplserviceService } from './pplservice.service';

@Component({
  selector: 'ppl',
  templateUrl: './ppl.page.html',
  styleUrls: ['./ppl.page.scss'],
})
export class PplPage implements OnInit {

  equipas:{};
  imagens: Image[];

  constructor(private pplserviceService: PplserviceService) { }

  ngOnInit() { 
      this.imagens = IMAGENS;
      this.pplserviceService.getEquipas().subscribe(equipas => this.equipas = equipas)  
  }
  getImagem(id){
    for(let e of this.imagens){
      if (e.id == id){
        return e.imgUrl;
      }
    }
  }
}
