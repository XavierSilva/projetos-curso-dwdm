import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PplPage } from './ppl.page';

describe('PplPage', () => {
  let component: PplPage;
  let fixture: ComponentFixture<PplPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PplPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PplPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
