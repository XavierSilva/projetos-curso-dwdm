create schema Escola;

use Escola;

create table Curso(
	codCurso char(4) primary key not null,
    nome varchar(60) not null,
    nrVagas int(3) not null
);

create table Professor(
	idProfessor char(4) primary key not null,
    nrProcesso int(4) not null,
    nome varchar(60) not null,
    dtNascimento date not null,
    morada varchar(60) not null
    );

create table Disciplina(
	codDisciplina char(4) primary key not null,
    nome varchar(60) not null,
    ano varchar(9) not null,
    tipoAvaliacao char not null CHECK(tipoAvaliacao IN (‘F’,’C’)),
    codCurso char(4), foreign key (codCurso) references Curso(codCurso),
    idProfessor char(4), foreign key (idProfessor) references Professor(idProfessor)
);
	
create table Aluno(
	idAluno  char(4)primary key not null,
	nrProcesso int(4) not null ,
    nome varchar(60) not null,
    dtNascimento date not null,
    morada varchar(60) not null,
    codCurso char(4) not null, foreign key (codCurso) references Curso(codCurso)
);

create table Nota(	
	idAluno char(4) not null, foreign key (idAluno) references Aluno(idAluno),
    codDisciplina char(4) , foreign key (codDisciplina) references Disciplina(codDisciplina),
    modalidade  CHAR not null CHECK(modalidade IN (‘N’,’R’)),
    classificacao  VARCHAR(3) not null CHECK(classificacao IN (‘0’,’1’,‘2’,’3’,‘4’,’5’,‘6’,’7’,‘8’,’9’,‘10’,’11’,‘12’,’13’,‘14’,’15’,‘16’,’17’,‘18’,’19’,‘20’,’D’,‘F’))
    
    
  
);
