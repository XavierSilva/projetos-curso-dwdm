use Escola;
#--> 1 <-- ============================================================

DELIMITER //
CREATE PROCEDURE GetAlunos()
BEGIN
SELECT * FROM Aluno order by codCurso;
END //
DELIMITER ;

call GetAlunos();
#========================================================================


#--> 2 <-- ============================================================

DELIMITER //
CREATE PROCEDURE GetDisciplinaCurso(in curso varchar(2))
BEGIN
select * from Disciplina where codCurso = curso;
END //
DELIMITER ;

call GetDisciplinaCurso('01');
#========================================================================


#--> 3 <-- ============================================================

DELIMITER //
CREATE procedure taxa()
BEGIN
SELECT p.nome as Nome, d.nome as Disciplina , count(n.classificacao) as Incricoes, count(case when n.classificacao > 10 then n.classificacao end) as Aprovados, (count(case when n.classificacao > 10 then n.classificacao end)  / count(n.classificacao)*100 ) as Percentagem
from nota as n, disciplina as d, professor as p
where  n.codDisciplina = d.codDisciplina AND d.idProfessor=p.idProfessor 
group by n.codDisciplina
order by Percentagem desc
limit 2;
END //
DELIMITER ;

call taxa();

#========================================================================



#--> 4 <-- ============================================================


DELIMITER //
CREATE PROCEDURE mostrarTodasNotas()
BEGIN
	select a.nome, c.nome, d.nome, p.nome, n.classificacao
	from aluno as a, curso as c, disciplina as d, professor as p, nota as n
	where  c.codCurso = d.codCurso and d.idProfessor = p.idProfessor and n.idAluno = a.idAluno and n.codDisciplina = d.codDisciplina
	order by a.nome asc ;
END //
DELIMITER ;

call mostrarTodasNotas();
#========================================================================


#--> 5 <-- ============================================================

DELIMITER //
CREATE PROCEDURE minAvgMaxIdades()
BEGIN
select d.nome,
 min(timestampdiff(year,a.dtNascimento,now())) as Minimo,
 avg (timestampdiff(year,a.dtNascimento,now())) as Media ,
 max(timestampdiff(year,a.dtNascimento,now())) as Maximo
 from disciplina as d, aluno as a, curso as c
where a.codCurso = c.codCurso and c.codCurso = d.codCurso
group by d.codDisciplina;
END //
DELIMITER ;

call minAvgMaxIdades();
#========================================================================


#--> 6 <-- ============================================================

DELIMITER //
CREATE PROCEDURE verNotasFiltro(
IN p_filtro VARCHAR(20),
IN p_valor VARCHAR(20)
)
BEGIN
if(p_filtro="curso") then
	select c.nome as Curso, d.nome as Disciplina,  a.nome as Aluno, n.classificacao as Nota, n.modalidade as Modalidade, d.tipoAvaliacao
	from aluno as a, nota as n, curso as c, disciplina as d
	where a.idAluno = n.idAluno  and n.codDisciplina = d.codDisciplina and d.codCurso = c.codCurso and c.codCurso=p_valor;
    
elseif(p_filtro="modalidade") then
	select c.nome as Curso, d.nome as Disciplina,  a.nome as Aluno, n.classificacao as Nota, n.modalidade as Modalidade,  d.tipoAvaliacao
	from aluno as a, nota as n, curso as c, disciplina as d
	where a.idAluno = n.idAluno  and n.codDisciplina = d.codDisciplina and d.codCurso = c.codCurso and n.modalidade=p_valor;    
    
elseif (p_filtro="tipo") then
    select c.nome as Curso, d.nome as Disciplina,  a.nome as Aluno, n.classificacao as Nota, n.modalidade as Modalidade,  d.tipoAvaliacao
	from aluno as a, nota as n, curso as c, disciplina as d
	where a.idAluno = n.idAluno  and n.codDisciplina = d.codDisciplina and d.codCurso = c.codCurso and d.tipoAvaliacao=p_valor;
end  if;
END //
DELIMITER ;

call verNotasFiltro("tipo","F");
call verNotasFiltro("modalidade","R");
call verNotasFiltro("curso","02");
#========================================================================
