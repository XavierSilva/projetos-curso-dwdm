use Escola;
# Cursos =============================================================

DELIMITER //
CREATE PROCEDURE inserirCurso(in codCurso varchar(2), in nome varchar(40), in  nrvagas int(2))
BEGIN
insert into Curso values
(codCurso,nome,nrVagas);
END //
DELIMITER ;

call inserirCurso("01","DWDM",30);
call inserirCurso("02","Engenharia Informatica",28);
#====================================================================


# Professores =============================================================

DELIMITER //
CREATE PROCEDURE inserirProfessor(in idProfessor varchar(2),in nrProcesso int(4), in nome varchar(60),in dtNascimento date, in  morada varchar(60))
BEGIN
insert into Professor values
(idProfessor, nrProcesso,  nome, dtNascimento,morada );
END //
DELIMITER ;

call inserirProfessor("01","0001","Alberto Ribeiro","1978/01/19","Rua X");
call inserirProfessor("02","0002","Jose Silva","1985/02/21","Rua Y");
call inserirProfessor("03","0003","Rute Mendes","1979/05/03","Rua Z");
#====================================================================


# Disciplinas =============================================================

DELIMITER //
CREATE PROCEDURE inserirDisciplina(in codDisciplina varchar(2),in nome varchar(60),in ano varchar(9), in  tipo char, in codCurso varchar(4), in idProfessor varchar(4))
BEGIN
insert into Disciplina values
(codDisciplina ,nome , ano,  tipo ,  codCurso ,  idProfessor);
END //
DELIMITER ;

call inserirDisciplina ("01","Matematica","2018/2019","F","01","01");
call inserirDisciplina ("02","Base de Dados","2018/2019","F","02","02");
call inserirDisciplina ("03","Algebra","2018/2019","C","01","03");
call inserirDisciplina ("04","CMS","2018/2019","C","02","03");
#====================================================================


# Alunos =============================================================

DELIMITER //
CREATE PROCEDURE inserirAluno(in idAluno varchar(4), in nrProcesso int(4),in nome varchar(60),in dtNascimento date,in morada varchar(60),in codCurso varchar(4))
BEGIN
insert into Aluno values
(idAluno ,nrProcesso , nome,  dtNascimento ,morada,  codCurso );
END //
DELIMITER ;

call inserirAluno ("01","0001","Aluno A","2001/01/11","Porto","01");
call inserirAluno ("02","0002","Aluno B","1997/04/22","Felguerias","02");
call inserirAluno ("03","0003","Aluno C","1995/02/05","Braga","02");
call inserirAluno ("04","0004","Aluno D","1998/05/03","Fafe","01");
call inserirAluno ("05","0005","Aluno E","1996/10/10","Porto","01");
call inserirAluno ("06","0006","Aluno F","1998/06/12","Guimaraes","02");
call inserirAluno ("07","0007","Aluno G","1999/05/21","Porto","01");
call inserirAluno ("08","0008","Aluno H","2000/01/16","Felguerias","01");
call inserirAluno ("09","0009","Aluno I","1994/02/04","Braga","02");
call inserirAluno ("10","0010","Aluno J","1989/03/20","Felguerias","02");
#====================================================================


# Notas =============================================================

DELIMITER //
CREATE PROCEDURE inserirNota (in p_idAluno varchar(4), in p_codDisciplina varchar(4), in p_modalidade char,in p_classificacao varchar(2))
BEGIN
DECLARE idCursoAluno, idCursoDisc INT DEFAULT 0;
select codCurso into idCursoAluno from aluno where idAluno=p_idAluno;
select codCurso into idCursoDisc from disciplina where codDisciplina=p_codDisciplina;

if(idCursoAluno = idCursoDisc) then
	insert into Nota values(p_idAluno ,p_codDisciplina , p_modalidade,  p_classificacao );
      
end if;
END //
DELIMITER ; 
call inserirNota ("01","01","N","10");
call inserirNota ("01","03","N","09");

call inserirNota ("02","02","N","01");
call inserirNota ("02","04","N","11");

call inserirNota ("03","02","N","11");
call inserirNota ("03","04","R","12");

call inserirNota ("04","01","N","12");
call inserirNota ("04","03","N","08");

call inserirNota ("05","01","N","10");
call inserirNota ("05","03","N","18");

call inserirNota ("06","02","N","06");
call inserirNota ("06","04","R","05");

call inserirNota ("07","01","N","17");
call inserirNota ("07","03","R","17");

call inserirNota ("08","01","R","10");
call inserirNota ("08","03","R","10");

call inserirNota ("09","02","N","15");
call inserirNota ("09","04","R","12");

call inserirNota ("10","02","N","06");
call inserirNota ("10","04","R","10");
#====================================================================