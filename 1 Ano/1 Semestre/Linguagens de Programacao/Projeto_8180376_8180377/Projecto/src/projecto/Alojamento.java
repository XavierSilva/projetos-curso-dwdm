package projecto;

import java.io.Serializable;
import java.util.Objects;

public class Alojamento implements Serializable{
    // Propriedades
    private Proprietario proprietario;
    private String nome;
    private String localizacao;
    private String tipologia;
    private String servicos;
    private int preco;
    private boolean disponiblidade;
    // Contrutores
    public Alojamento() {
    }
    public Alojamento(Proprietario proprietario, String nome, String localizacao, String tipologia, String servicos, int preco, boolean disponiblidade) {
        this.proprietario = proprietario;
        this.nome = nome;
        this.localizacao = localizacao;
        this.tipologia = tipologia;
        this.servicos = servicos;
        this.preco = preco;
        this.disponiblidade = disponiblidade;
    }
    // Getters e Setters

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getLocalizacao() {
        return localizacao;
    }
    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }
    public String getTipologia() {
        return tipologia;
    }
    public void setTipologia(String tipologia) {
        this.tipologia = tipologia;
    }
    public String getServicos() {
        return servicos;
    }
    public void setServicos(String servicos) {
        this.servicos = servicos;
    }
    public int getPreco() {
        return preco;
    }
    public void setPreco(int preço) {
        this.preco = preço;
    }
    
    public boolean isDisponiblidade() {
        return disponiblidade;
    }
    public void setDisponiblidade(boolean disponiblidade) {
        this.disponiblidade = disponiblidade;
    }
    public Proprietario getProprietario() {
        return proprietario;
    }
    public void setProprietario(Proprietario proprietario) {
        this.proprietario = proprietario;
    }
    // equals
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Alojamento other = (Alojamento) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.proprietario, other.proprietario)) {
            return false;
        }
        return true;
    }
    // toString's
    @Override
    public String toString() {
        return "Alojamento{" + "proprietario=" + proprietario.toString() + ", nome=" + nome + ", localizacao=" + localizacao + ", tipologia=" + tipologia + ", servicos=" + servicos + ", preço=" + preco + ", disponiblidade=" + disponiblidade + '}';
    }public String toStringNomeAlojamento() {
        return "\n" + nome + ";";
    }
    public String toStringParaHospede(){
        return "\n" + nome + ", localizacao=" + localizacao + ", tipologia=" + tipologia + ", servicos=" + servicos + ", preço=" + preco + ", disponiblidade=" + disponiblidade + '}';
    }


    
    
}
