/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecto;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author ManuelFernandoSilvaR
 */
public class Armazenar {
    
    
   
    
     public int salvarDados(Lista listas ){
        try{
            
            FileOutputStream fs = new FileOutputStream("listas.arq"); //tanto faz o nome do arquivo
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(listas); //referencia a estrutura que se quer armazenar
            os.close( );
            System.out.println("Dados Salvos com Sucesso!");
            return 0;
        }catch(Exception ex){
            return -1;
            
        }
    }   
     
     
      public Lista lerDados(String fich ){
        try{
            ArrayList<Alojamento> alojamentos = new ArrayList();
            ArrayList<Hospede> hospedes = new ArrayList();
            ArrayList<Proprietario> proprietarios = new ArrayList();
            ArrayList<Reserva> reservas = new ArrayList();
        
        
            Lista listas= new Lista(alojamentos,hospedes,proprietarios,reservas);
            
            
            FileInputStream fs = new FileInputStream(fich); //tanto faz o nome do arquivo
            ObjectInputStream os = new ObjectInputStream(fs);
           
            listas=(Lista)os.readObject(); //referencia a estrutura que se quer armazenar
            os.close( );
            System.out.println("Dados Carregados com Sucesso!");
            return listas;
        }catch(Exception ex){
            return null;
            
        }
    }   
     
     
     
}
    
    
    
    

