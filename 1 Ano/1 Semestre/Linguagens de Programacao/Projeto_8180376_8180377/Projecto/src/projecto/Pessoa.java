
package projecto;

import java.io.Serializable;
import java.util.Objects;


public class Pessoa implements Serializable {
    //Propriedades
    private String nome;
    private String morada;
    private String email;
    private String contacto;
    private String password;
    
    // Getters e Setters
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    // Construtores
    public Pessoa() {
    }

    public Pessoa(String nome, String morada, String email, String contacto, String password) {
        this.nome = nome;
        this.morada = morada;
        this.email = email;
        this.contacto = contacto;
        this.password = password;
    }
    
    // Equals
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pessoa other = (Pessoa) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        return true;
    }
    // toString
    @Override
    public String toString() {
        return "Pessoa{" + "nome=" + nome + ", morada=" + morada + ", email=" + email + ", contacto=" + contacto + ", password=" + password + '}';
    }




  
                    
    
}
