
package projecto;

import java.io.Serializable;
import java.util.*;

public class Lista implements Serializable{
    // Listas:
    private ArrayList<Alojamento> alojamentos = new ArrayList();
    private ArrayList<Hospede> hospedes = new ArrayList();
    private ArrayList<Proprietario> proprietarios = new ArrayList();
    private ArrayList<Reserva> reservas = new ArrayList();
    // Construtores:
    public Lista(ArrayList<Alojamento> alojamentos,ArrayList<Hospede> hospedes,ArrayList<Proprietario> proprietarios,ArrayList<Reserva> reservas) {
        this.alojamentos = alojamentos;
        this.proprietarios=proprietarios;
        this.hospedes=hospedes;
        this.reservas = reservas;
        
    }
    public Lista(){
        
    }
    // Métodos para Proprietario:
    public void adicionarProprietario(Proprietario p){
       if(proprietarios.contains(p)) {
           System.out.println("Já existe esse Proprietario!");
       }else{
           proprietarios.add(p);
           System.out.println("Proprietario adicionado!");
           System.out.println(p.toString());
       }    
    }   
    public void pesquisarProprietario(Proprietario p){
        int posicao = proprietarios.indexOf(p);
        if(posicao == -1 ) {
           System.out.println("Não existe esse Proprietario!");
       }else{            
          Proprietario p1 = new Proprietario();
          p1= proprietarios.get(posicao);
          System.out.println(p1.toString());
       } 
    }    
    public void alterarProprietario(Proprietario p){
        Scanner leitura = new Scanner(System.in);
        String morada,email,contacto;
        int posicao = proprietarios.indexOf(p);
        if(posicao == -1 ) {
           System.out.println("Não existe esse Proprietario!");
       }else{            
          Proprietario p1 = new Proprietario();
          System.out.println("Redefina a morada:");
          morada = leitura.nextLine();
          System.out.println("Redefina o email:");
          email = leitura.nextLine();
          System.out.println("Redefina o contacto:");
          contacto = leitura.nextLine();
          
          p1.setNome(p.getNome());
          p1.setPassword(p.getPassword());
          p1.setMorada(morada);
          p1.setEmail(email);
          p1.setContacto(contacto);
          proprietarios.set(posicao, p1);
          System.out.println (p1.toString());
       }
    }
    public void eliminarProprietario(Proprietario p){
        Scanner leitura = new Scanner(System.in);
        String morada,email,contacto;
        int posicao = proprietarios.indexOf(p);
        if(posicao == -1 ) {
           System.out.println("Não existe esse Proprietario!");
       }else{            
          
          proprietarios.remove(posicao);
            System.out.println("Proprietário Eliminado");
       }
    }
    public int loginProprietario(Proprietario p){
        int res = -1;
        if(proprietarios.contains(p)) {
            res = proprietarios.indexOf(p);
           System.out.println("Login Com Sucesso!");
           
       }else{
            System.out.println("Proprietario ou Password Errada!");
       }  
        return res;
    }
    public void mostrarRendimentos(Proprietario p){
        Proprietario proprietario = new Proprietario();
        proprietario = null;
        for ( int i = 0; i <= proprietarios.size()-1; i++){
            proprietario = proprietarios.get(i);   
            if(proprietario.equals(p)){
                System.out.println("\n Rendimentos: "+ proprietario.getRentablizacao() + "€. \n Total de Resevas: "+ proprietario.getTotalReservas());
            }
        }
    }
    public Proprietario obterDadosProprietario(Proprietario p){
        Proprietario proprietario = new Proprietario();
        proprietario = null;
        for ( int i = 0; i <= proprietarios.size()-1; i++){
            proprietario = proprietarios.get(i);   
            if(proprietario.equals(p)){
                proprietario = proprietarios.get(i);
            }
        }
        return proprietario;
    }
    public void adicionarRentalizacao(Proprietario p, int dias, int preco){
        Proprietario proprietario = new Proprietario();
        proprietario = null;
        int rentablizacao = 0;
        for ( int i = 0; i <= proprietarios.size()-1; i++){
            proprietario = proprietarios.get(i);   
            if(proprietario.equals(p)){
                rentablizacao = preco * dias;
                proprietario = proprietarios.get(i);
                proprietario.setRentablizacao(proprietario.getRentablizacao() + rentablizacao);
                proprietario.setTotalReservas(proprietario.getTotalReservas() + 1);
                proprietarios.set(i, proprietario);
            }
        }
    }
    public void retirarRentablizacao(Proprietario p, int dias, int preco){
        Proprietario proprietario = new Proprietario();
        proprietario = null;
        int rentablizacao = 0;
        for ( int i = 0; i <= proprietarios.size()-1; i++){
            proprietario = proprietarios.get(i);   
            if(proprietario.equals(p)){
                rentablizacao = preco * dias;
                proprietario = proprietarios.get(i);
                proprietario.setRentablizacao(proprietario.getRentablizacao() - rentablizacao);
                proprietario.setTotalReservas(proprietario.getTotalReservas()- 1);
                proprietarios.set(i, proprietario);
            }
        }
    }
    
    // Métodos para Hospede:    
    public void adicionarHospede(Hospede h){
       if(hospedes.contains(h)) {
           System.out.println("Já existe esse Proprietario!");
       }else{
           hospedes.add(h);
           System.out.println("Hospede adicionado!");
           System.out.println(h.toString());
       }    
    }   
    public void pesquisarHospede(Hospede h){
        int posicao = hospedes.indexOf(h);
        if(posicao == -1 ) {
           System.out.println("Não existe esse Hospede!");
       }else{            
          Hospede h1 = new Hospede();
          h1= hospedes.get(posicao);
          System.out.println(h1.toString());
       } 
    }    
    public void alterarHospede(Hospede h){
        Scanner leitura = new Scanner(System.in);
        String morada,email,contacto;
         int nif,cartaocid;
        int posicao = hospedes.indexOf(h);
        if(posicao == -1 ) {
           System.out.println("Não existe esse Hospede!");
       }else{            
          Hospede h1 = new Hospede();
          System.out.println("Redefina a morada:");
          morada = leitura.nextLine();
          System.out.println("Redefina o email:");
          email = leitura.nextLine();
          System.out.println("Redefina o contacto:");
          contacto = leitura.nextLine();
          System.out.println("Redefina o NIF:");
          nif = leitura.nextInt();
          System.out.println("Redefina o Numero CC:");
          cartaocid = leitura.nextInt();
          
          h1.setNome(h.getNome());
          h1.setPassword(h.getPassword());
          h1.setMorada(morada);
          h1.setEmail(email);
          h1.setContacto(contacto);
          h1.setCartaocid(cartaocid);
          h1.setNif(nif);
          
          hospedes.set(posicao, h1);
          System.out.println (h1.toString());
       }
    }
    public void eliminarHospede(Hospede h){
        Scanner leitura = new Scanner(System.in);
        int posicao = hospedes.indexOf(h);
        if(posicao == -1 ) {
           System.out.println("Não existe esse Hospede!");
       }else{            
          
          hospedes.remove(posicao);
            System.out.println("Hospede Eliminado");
       }
    }   
    public int loginHospede(Hospede h){
        int res = -1;
        if(hospedes.contains(h)) {
           res = hospedes.indexOf(h);
           System.out.println("Login Com Sucesso!");
           
       }else{
            System.out.println("Hospede ou Password Errada!");
       }  
        return res;
    }
    public Hospede obterDadosHospede(Hospede h){
        Hospede hospede = new Hospede();
        hospede = null;
        for ( int i = 0; i <= hospedes.size()-1; i++){
            hospede = hospedes.get(i);   
            if(hospede.equals(h)){
                hospede = hospedes.get(i);
            }
        }
        return hospede;
    }
    
    // Métodos para Alojamento
    public void adicionarAlojamento(Alojamento a){
       if(alojamentos.contains(a)) {
           System.out.println("Já existe esse Alojamento!");
       }else{
           alojamentos.add(a);
           System.out.println("Alojamento adicionado!");
           System.out.println(a.toString());
       }    
    }   
    public void pesquisarAlojamento(Alojamento a){
        int posicao = alojamentos.indexOf(a);
        if(posicao == -1 ) {
           System.out.println("Não existe esse Alojamento!");
       }else{            
          Alojamento a1 = new Alojamento();
          a1= alojamentos.get(posicao);
          System.out.println(a1.toString());
       } 
    }    
    public void alterarAlojamento(Alojamento a){
        Scanner leitura = new Scanner(System.in);
        String nome,localizacao,tipologia,servicos;
        int preco;
        int posicao = alojamentos.indexOf(a);
        if(posicao == -1 ) {
           System.out.println("Não existe esse Proprietario!");
       }else{            
          Alojamento a1 = new Alojamento();
          System.out.println("Redefina o nome:");
          nome = leitura.nextLine();
          System.out.println("Redefina a localizacao:");
          localizacao = leitura.nextLine();
          System.out.println("Redefina a tipologia:");
          tipologia = leitura.nextLine();
          System.out.println("Redefina os servicos:");
          servicos = leitura.nextLine();
          System.out.println("Redefina o preco:");
          preco = leitura.nextInt();
          a1.setNome(nome);
          a1.setLocalizacao(localizacao);
          a1.setTipologia(tipologia);
          a1.setServicos(servicos);
          a1.setPreco(preco);
          alojamentos.set(posicao, a1);
          System.out.println (a1.toString());
       }
    }
    public void eliminarAlojamento(Alojamento a){
        int posicao = alojamentos.indexOf(a);
        if(posicao == -1 ) {
           System.out.println("Não existe esse Alojamento!");
       }else{            
          
          alojamentos.remove(posicao);
            System.out.println("Alojamento Eliminado");
       }
    }
    public void mostrarAlojamentos(Proprietario p){
        System.out.print("Alojamentos:\n");
        for ( int i = 0; i <= alojamentos.size()-1; i++){
            Alojamento a = alojamentos.get(i);            
            if( p.equals(a.getProprietario())){
                System.out.print(a.toStringNomeAlojamento());
            }
        }
    } 
    public void pesquisarAlojamentos(){
        System.out.print("Alojamentos:");
        for ( int i = 0; i <= alojamentos.size()-1; i++){
            Alojamento a = alojamentos.get(i);         
            System.out.print(a.toStringParaHospede());            
        }
    }
    public void pesquisarAlojamentos(Boolean disp){
        int numResultado = 0;
        System.out.print("Alojamentos Disponiveis:");
        for ( int i = 0; i <= alojamentos.size()-1; i++){
            Alojamento a = alojamentos.get(i);         
            if(a.isDisponiblidade() == disp){
                numResultado ++;
                System.out.print(a.toStringParaHospede());            
            }
        }        
        if(numResultado == 0){
            System.out.println("Não encontramos a sua pesquisa!");
        }        
    }
    public void pesquisarAlojamentos(int preco){
        int numResultado = 0;
        System.out.print("Alojamentos:");
        for ( int i = 0; i <= alojamentos.size()-1; i++){
            Alojamento a = alojamentos.get(i);
            if(a.getPreco() <= preco){
                numResultado ++;
                System.out.print(a.toStringParaHospede());            
            }
        }        
        if(numResultado == 0){
            System.out.println("Não encontramos a sua pesquisa!");
        }
    }
    public void pesquisarAlojamentosLocal(String local){
        int numResultado = 0;
        System.out.print("Alojamentos:");
        for ( int i = 0; i <= alojamentos.size()-1; i++){
            Alojamento a = alojamentos.get(i);
            if(a.getLocalizacao().equals(local)){
                numResultado ++;
                System.out.print(a.toStringParaHospede());            
            }            
        }
        if(numResultado == 0){
            System.out.println("Não encontramos a sua pesquisa!");
        }
    }
    public void pesquisarAlojamentosTipo(String tipologia){
        int numResultado = 0;
        System.out.print("Alojamentos:");
        for ( int i = 0; i <= alojamentos.size()-1; i++){
            Alojamento a = alojamentos.get(i);
            if(a.getTipologia().equals(tipologia)){
                numResultado ++;
                System.out.print(a.toStringParaHospede());            
            }            
        }
        if(numResultado == 0){
            System.out.println("Não encontramos a sua pesquisa!");
        }
    }
    public void pesquisarAlojamentosServico(String servico){
        int numResultado = 0;
        System.out.print("Alojamentos:");
        for ( int i = 0; i <= alojamentos.size()-1; i++){
            Alojamento a = alojamentos.get(i);
            if(a.getServicos().equals(servico)){
                numResultado ++;
                System.out.print(a.toStringParaHospede());            
            }           
        }
        if(numResultado == 0){
            System.out.println("Não encontramos a sua pesquisa!");
        }
    }
    public Alojamento obterDadosAloj(String nomeAlojamento){
        Alojamento res = new Alojamento();
        Alojamento aloj = new Alojamento();
        aloj = null;
        for ( int i = 0; i <= alojamentos.size()-1; i++){
            aloj = alojamentos.get(i);   
            if(nomeAlojamento.equals(aloj.getNome())){
                res = alojamentos.get(i);
            }
        }
        return res;
    }
    
    // Métodos para Reserva
    public void adicionarReserva(Reserva r){
        
       if(reservas.contains(r)) {
           System.out.println("Não existe esse Alojamento!");
       }else{
           Alojamento aloj = new Alojamento();
           aloj = (r.getAlojamento());
           if(aloj.isDisponiblidade() == true){
                System.out.println("Reserva adicionada!");           
                aloj = (r.getAlojamento());
                int posicao = alojamentos.indexOf(aloj);
                aloj.setDisponiblidade(false);
                alojamentos.set(posicao,aloj);
                System.out.println(r.toString());
           }else{
               System.out.println("Este Alojamento já se encontra sob Reserva");
           }
           reservas.add(r);
           
           
       }    
    }
    public void cancelarReserva(String nomealoj, Hospede h){
        Reserva r = new Reserva();
        Alojamento a = new Alojamento();
        Proprietario p = new Proprietario();
        int res= 0;
        for(int i = 0 ; i <= reservas.size()-1; i++){
            r = reservas.get(i);
            if(r.getHospede().equals(h)){
                a = r.getAlojamento();
                if(a.getNome().equals(nomealoj)){                    
                    p = a.getProprietario();
                    retirarRentablizacao(p,r.getDias(),a.getPreco());
                    reservas.remove(i);
                    a.setDisponiblidade(true);
                    System.out.println("Reserva Cancelada!");
                    res ++;
                }
            }
        }
        if(res == 0){
            System.out.println("Não tem reservas neste Alojamento!");
        }
    }
    public void finalizarReserva(Proprietario p){
        Scanner captura = new Scanner(System.in);
        Reserva r = new Reserva();
        Alojamento a = new Alojamento();
        String nomeAloj ="";
        int res = 0;
        int dias, preco;
        System.out.println("Qual o nome do Alojamento Reservado?");
        nomeAloj = captura.nextLine();
        for(int i = 0 ; i <= reservas.size()-1; i++){
            r = reservas.get(i);
            a = r.getAlojamento();
            if(a.getProprietario().equals(p)){                
                if(a.getNome().equals(nomeAloj)){ 
                    if(r.getEstado() == 1){
                        r.setEstado(0);
                        reservas.set(i, r);
                        preco = a.getPreco();
                        dias = r.getDias();
                        a.setDisponiblidade(true);
                        adicionarRentalizacao(p,dias,preco);
                        System.out.println("Reserva Finalizada!");
                        res ++;
                    }
                    
                }
            }
        }
        if ( res == 0){
            System.out.println("Não tem reservas neste alojamento");
        }
    }

    public void pesquisarReserva(Reserva r){
        int posicao = reservas.indexOf(r);
        if(posicao == -1 ) {
           System.out.println("Não existe essa Reserva!");
       }else{            
          Reserva r1 = new Reserva();
          r1= reservas.get(posicao);
          System.out.println(r1.toString());
       } 
    }    
    public void alterarReserva(Reserva r, Hospede h, Proprietario p){
        Scanner leitura = new Scanner(System.in);
        int dias;
        int posicao = reservas.indexOf(r);
        if(posicao == -1 ) {
           System.out.println("Não existe essa Reserva!");
       }else{            
          Reserva r1 = new Reserva();
          System.out.println("Redefina o numero de dias:");
          dias = leitura.nextInt();
          r1.setDias(dias);
          reservas.set(posicao, r1);
          System.out.println (r1.toString());
       }
    }
    public void eliminarReserva(Reserva r){
        Scanner leitura = new Scanner(System.in);
        int posicao = proprietarios.indexOf(r);
        if(posicao == -1 ) {
           System.out.println("Não existe essa Reserva!");
       }else{            
          
          reservas.remove(posicao);
            System.out.println("Reserva Eliminada");
       }
    }
    public void mostrarReservas(Proprietario p){
        int numResultado = 0;
        Alojamento aloj = new Alojamento();
        Reserva res = new Reserva();
        Proprietario pro = new Proprietario();
        for( int i = 0; i <= reservas.size()-1; i++){
            res = reservas.get(i);
            aloj = res.getAlojamento();
            pro = aloj.getProprietario();
            if(pro.equals(p)){
                numResultado ++;
                System.out.println(res.toString());
            }
        }
        if(numResultado == 0){
            System.out.println("Não existem reservas nos seu alojamentos!");
        }
    }
    public void mostrarReservasAtivas(Proprietario p){
        int numResultado = 0;
        Alojamento aloj = new Alojamento();
        Reserva res = new Reserva();
        Proprietario pro = new Proprietario();
        for( int i = 0; i <= reservas.size()-1; i++){
            res = reservas.get(i);
            aloj = res.getAlojamento();
            pro = aloj.getProprietario();
            if(pro.equals(p)){
                if(res.getEstado()== 1){
                    numResultado ++;
                    System.out.println(res.toString());
                }                
            }
        }
        if(numResultado == 0){
            System.out.println("Não existem reservas Ativas!");
        }
    }
     public void mostrarReservasFinalizadas(Proprietario p){
        int numResultado = 0;
        Alojamento aloj = new Alojamento();
        Reserva res = new Reserva();
        Proprietario pro = new Proprietario();
        for( int i = 0; i <= reservas.size()-1; i++){
            res = reservas.get(i);
            aloj = res.getAlojamento();
            pro = aloj.getProprietario();
            if(pro.equals(p)){
                if(res.getEstado()== 0){
                    numResultado ++;
                    System.out.println(res.toString());
                }
                
            }
        }
        if(numResultado == 0){
            System.out.println("Não existem reservas Finalizadas!");
        }
    }
     
    // Métodos Auxiliares
    public void mostrarListas(){
        /*
        for ( int i = 0; i <= alojamentos.size()-1; i++){
            Alojamento a = alojamentos.get(i);            
            System.out.println(a.toString());            
        }
        for ( int i = 0; i <= proprietarios.size()-1; i++){
            Proprietario a = proprietarios.get(i);            
            System.out.println(a.toString());            
        }
        for ( int i = 0; i <= reservas.size()-1; i++){
            Reserva a = reservas.get(i);            
            System.out.println(a.toString());            
        }
        for ( int i = 0; i <= hospedes.size()-1; i++){
            Hospede a = hospedes.get(i);            
            System.out.println(a.toString());            
        }*/
        System.out.println("P: " + proprietarios.size());
        System.out.println("A: " + alojamentos.size());
        System.out.println("H: " + hospedes.size());
        System.out.println("R: " + reservas.size());
    }
    public Proprietario obterProprietario(String nomeAlojamento){
        Proprietario p = new Proprietario();
        Alojamento aloj = new Alojamento();
        p = null;
        for ( int i = 0; i <= alojamentos.size()-1; i++){
            aloj = alojamentos.get(i);   
            if(nomeAlojamento.equals(aloj.getNome())){
                p = aloj.getProprietario();
            }
        }
        return p;
    }
        
    
    
}


   
