/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Xavier && Joao
 */
public class Projecto {
     static int pedido = 0;

    /**
     * @param args the command line arguments
     */
    //Menus     
    public static void menuLoginProprietario(Lista listas){
        Scanner captura = new Scanner (System.in);        
        int pedido;
        do{
            System.out.println("");
            System.out.println("");
            System.out.println( "========== Menu Proprietários ==========" ) ;  
            System.out.println( "       Escolha uma opção: " ) ;  
            System.out.println( "1 - Login" ) ;  
            System.out.println( "2 - Registar" ) ;  
            System.out.println( "0 - Voltar" ) ; 
            System.out.println( "Selecione um número: " ) ; 
            pedido = captura.nextInt( ) ;  
            switch ( pedido ) {  
	            case 1:   
                        loginProprietario(listas);
                        break;                              
	            case 2:
                         adicionarProprietario(listas); 
                         gravar(listas);
	                break ;  
                    case 0:
                        break;
	            default:  
	                System.out.println( "Erro! Número não existe no menu!" ) ;  
	                break ;  
	        } 
        }while (pedido != 0);
        menu(listas);
    }
    public static void menuLoginHospedes(Lista listas){
        Scanner captura = new Scanner (System.in);        
        int pedido;
        do{
            System.out.println("");
            System.out.println("");
            System.out.println( "========== Menu Hospedes ==========" ) ;  
            System.out.println( "       Escolha uma opção: " ) ;  
            System.out.println( "1 - Login" ) ;  
            System.out.println( "2 - Registar" ) ;  
            System.out.println( "0 - Voltar" ) ; 
            System.out.println( "Selecione um número: " ) ; 
            pedido = captura.nextInt( ) ;  
            switch ( pedido ) {  
	            case 1:   
                        loginHospede(listas);
                        break;                              
	            case 2:
                         adicionarHospede(listas); 
                         gravar(listas);
	                break ;  
                    case 0:                         
                        break;
	            default:  
	                System.out.println( "Erro! Número não existe no menu!" ) ;  
	                break ;  
	        } 
        }while (pedido != 0);
                gravar(listas);
    }
    public static void menuProprietarioLvl2(Lista listas,Proprietario p){
        Scanner captura = new Scanner (System.in);        
        int pedido;
        listas.mostrarAlojamentos(p);
        do{
            System.out.println("");
            System.out.println("");
            System.out.println( "========== Menu Proprietários ==========" ) ;  
            System.out.println( "       Escolha uma opção: " ) ;  
            System.out.println( "1 - Adicionar Alojamento " ) ;  
            System.out.println( "2 - Pesquisar Alojamento" ) ;  
            System.out.println( "3 - Alterar Alojamento" ) ;  
            System.out.println( "4 - Eliminar Alojamento" ) ; 
            System.out.println( "0 - Voltar" ) ; 
            System.out.println( "Selecione um número: " ) ; 
            pedido = captura.nextInt( ) ;  
            switch ( pedido ) {  
	            case 1:   
                        adicionarAlojamento(listas,p);
                        break;                              
	            case 2:
                         pesquisarAlojamento(listas,p);                        
	                break ;  
	            case 3:
                        alterarAlojamento(listas,p);
	                break ;                          
	            case 4:
                        listas.mostrarAlojamentos(p);
                        eliminarAlojamento(listas,p);                
	                break ;
                    case 0:
                        break;
	            default:  
	                System.out.println( "Erro! Número não existe no menu!" ) ;  
	                break ;  
	        } 
        }while (pedido != 0);             
    }
    public static void menuProprietarioLvl1(Lista listas,Proprietario p){
        Scanner captura = new Scanner (System.in);        
        do{
            System.out.println("");
            System.out.println("");
            System.out.println( "========== Menu Proprietários ==========" ) ;  
            System.out.println( "       Escolha uma opção: " ) ;  
            System.out.println( "1 - Alojamentos " ) ;  
            System.out.println( "2 - Reservas");
            System.out.println( "3 - Editar Conta" ) ;  
            System.out.println( "4 - Mostrar Rendimentos e Total de Reservas");
            System.out.println( "0 - LogOut" ) ; 
            System.out.println( "Selecione um número: " ) ; 
            pedido = captura.nextInt( ) ;  
            switch ( pedido ) {  
	            case 1:   
                        menuProprietarioLvl2(listas,p);
                        break;
                    case 2:
                        menuReservas(listas,p);
                        break;
	            case 3:
                         listas.alterarProprietario(p);                        
	                break ;
                     case 4:
                         listas.mostrarRendimentos(p);                        
	                break ;
                    case 0:
                        break;
	            default:  
	                System.out.println( "Erro! Número não existe no menu!" ) ;  
	                break ;  
	        } 
        }while (pedido != 0);
        gravar(listas);
    }
    public static void menuReservas(Lista listas,Proprietario p){
        Scanner captura = new Scanner (System.in);        
        do{
            System.out.println("");
            System.out.println("");
            System.out.println( "========== Menu Proprietários ==========" ) ;  
            System.out.println( "       Escolha uma opção: " ) ;  
            System.out.println( "1 - Ver Todas Reservas " ) ;  
            System.out.println( "2 - Ver Reservas Ativas" ) ;
            System.out.println( "3 - Ver Reservas Finalizadas" ) ;  
            System.out.println( "4 - Finalizar Reserva");
            System.out.println( "0 - Voltar" ) ; 
            System.out.println( "Selecione um número: " ) ; 
            pedido = captura.nextInt( ) ;  
            switch ( pedido ) {  
	            case 1:   
                        listas.mostrarReservas(p);
                        break;                              
	            case 2:
                         listas.mostrarReservasAtivas(p);                        
	                break ;
                     case 3:
                         listas.mostrarReservasFinalizadas(p);                        
	                break ;
                     case 4:                         
                         listas.finalizarReserva(p);
                    case 0:
                        gravar(listas);
                        menuProprietarioLvl1(listas, p);                        
                        break;
	            default:  
	                System.out.println( "Erro! Número não existe no menu!" ) ;  
	                break ;  
	        } 
        }while (pedido != 0);
    }
    public static void menuAlojamento(Lista listas, Hospede h){
        Scanner captura = new Scanner (System.in);        
        int pedido;
        do{
            System.out.println("");
            System.out.println("");
            System.out.println( "========== Menu Hospedes ==========" ) ;  
            System.out.println( "       Escolha uma opção: " ) ;  
            System.out.println( "1 - Pesquisar Alojamentos" ) ;  
            System.out.println( "2 - Efetuar Reservar" ) ;
            System.out.println( "3 - Editar Conta" ) ;  
            System.out.println( "4 - Cancelar Reserva");
            System.out.println( "0 - LogOut" ) ; 
            System.out.println( "Selecione um número: " ) ; 
            pedido = captura.nextInt( ) ;  
            switch ( pedido ) {  
	            case 1:
                        menuPesquisaAvancada(listas,h);                       
                        break;                              
	            case 2:
                         adicionarReserva(listas,h);                        
	                break ;  
	            case 3:
                        listas.alterarHospede(h);
	                break ;  
                        
	            case 4:
                         cancelarReserva(listas,h);               
	                break ;
                    case 0:
                        break;      
	            default:  
	                System.out.println( "Erro! Número não existe no menu!" ) ;  
	                break ;  
	        } 
        }while (pedido != 0);  

    }
    public static void menuPesquisaAvancada(Lista listas, Hospede h){
        Scanner captura = new Scanner (System.in);        
        int pedido;
        do{
            System.out.println("");
            System.out.println("");
            System.out.println( "========== PESQUISA AVANÇADA ==========" ) ;  
            System.out.println( "       Escolha uma opção: " ) ;  
            System.out.println( "1 - Todos Alojamentos" ) ;  
            System.out.println( "2 - Por Localização:" ) ;
            System.out.println( "3 - Por Preço:" ) ;  
            System.out.println( "4 - Por Disponiblidade:" ) ; 
            System.out.println( "5 - Por Tipologia:" ) ; 
            System.out.println( "6 - Por Serviço:" ) ; 
            System.out.println( "0 - Voltar" ) ; 
            System.out.println( "Selecione um número: " ) ; 
            pedido = captura.nextInt( ) ;  
            switch ( pedido ) {  
	            case 1:
                        listas.pesquisarAlojamentos();
                        break;                              
	            case 2:
                        String local = "";
                        System.out.println("Qual a localização que pretende?");
                        local = captura.next();
                         listas.pesquisarAlojamentosLocal(local);
	                break ;  
	            case 3:
                        int preco = 0 ;
                        System.out.println("Qual o valor máximo que pretende?");
                        preco = captura.nextInt();
                        listas.pesquisarAlojamentos(preco); 
	                break ;  
                        
	            case 4:
                         listas.pesquisarAlojamentos(Boolean.TRUE);               
	                break ;
                    case 5:
                        String tipo = "";
                        System.out.println("Qual a tipologia do alojamento que procura?");
                        tipo = captura.next();
                         listas.pesquisarAlojamentosTipo(tipo);
                        break;
                    case 6:
                        String servico = "";
                        System.out.println("Qual o serviço que procura?");
                        servico = captura.next();
                         listas.pesquisarAlojamentosServico(servico);
                        break;
                    case 0:
                        break;      
	            default:  
	                System.out.println( "Erro! Número não existe no menu!" ) ;  
	                break ;  
	        } 
        }while (pedido != 0);
    }
    public static void menuHospede(Lista listas, Hospede h){
        Scanner captura = new Scanner (System.in);        
        do{
            System.out.println("");
            System.out.println("");
            System.out.println( "========== Menu Hospede ==========" ) ;  
            System.out.println( "       Escolha uma opção: " ) ;  
            System.out.println( "1 - Adicionar Hospede" ) ;  
            System.out.println( "2 - Pesquisar Hospede" ) ;  
            System.out.println( "3 - Alterar Hospede" ) ;  
            System.out.println( "4 - Eliminar Hospede" ) ; 
            System.out.println( "0 - Voltar" ) ; 
            System.out.println( "Selecione um número: " ) ; 
            pedido = captura.nextInt( ) ;  
            switch ( pedido ) {  
	            case 1:   
                        //adicionarHospede(listas);
                        break;                              
	            case 2:
                       // pesquisarHospede(listas);                    
	                break ;  
	            case 3:
                       // alterarHospede(listas);
	                break ;  
                        
	            case 4:
                         // eliminarHospede(listas)      ;          
	                break ;
                    case 0:
                        break;

	            default:  
	                System.out.println( "Erro! Número não existe no menu!" ) ;  
	                break ;  
	        } 
        }while (pedido != 0);         
    }
    public static void menu(Lista listas){
        
        Scanner captura = new Scanner (System.in);     
      
        do{ 
            System.out.println("");
            System.out.println("");
            System.out.println( "========== Menu Principal ==========" ) ;  
            System.out.println( "       Escolha uma opção: " ) ;  
            System.out.println( "1 - Area Proprietario" ) ;  
            System.out.println( "2 - Area hospede" ) ; 
            System.out.println( "3 - Ler Dados" ) ; 
            System.out.println( "4 - Guardar Dados" ) ;              
            System.out.println( "0 - Sair" ) ; 
            System.out.println( "Selecione um número: " ) ; 
            pedido = captura.nextInt( ) ;  
            switch ( pedido ) {  
	            case 1:   
                        menuLoginProprietario(listas);
                        break;                              
	            case 2:
                       menuLoginHospedes(listas);                      
	                break ;  
	            case 3:
                        listas = ler();
                        if(listas != null){
                            listas.mostrarListas(); 
                        }else{
                            System.out.println("Não foi possivel ler");
                        }                                               
	                break ;                       
	            case 4: 
                        gravar(listas); 
	                break;
                    case 0:
                        break;
	            default:  
	                System.out.println( "Erro! Número não existe no menu!" ) ;  
	                break ;  
	        } 
        }while (pedido != 0);

    }
    
    //Main
    public static void main(String[] args) {        
      Lista listas = new Lista();
      listas = ler();
      if(listas == null){         
        System.out.println("Não foi possivel ler");
        listas = new Lista();
      } 
      menu(listas);
    }
    
    // Métodos para Proprietario
    public static void adicionarProprietario(Lista lista){
        Scanner leitura = new Scanner(System.in);
        String nome, morada, email, contacto,pw;
        Proprietario p = new Proprietario();
        System.out.println("Qual o nome: ");
        nome = leitura.nextLine();
        System.out.println("Password: ");        
        pw = leitura.nextLine();        
        System.out.println("Morada: ");
        morada = leitura.nextLine();       
        System.out.println("Email: ");
        email = leitura.nextLine();       
        System.out.println("Contacto: ");        
        contacto = leitura.nextLine();
         p.setNome(nome);
         p.setMorada(morada);
         p.setContacto(contacto);
         p.setEmail(email);
         p.setPassword(pw);         
         lista.adicionarProprietario(p);
        
    }
    public static void pesquisarProprietario(Lista lista){
        Scanner leitura = new Scanner(System.in);
        String nome;
        Proprietario p = new Proprietario();
        System.out.println("Qual o nome: ");
        nome = leitura.nextLine();
        p.setNome(nome);
        lista.pesquisarProprietario(p);
    }
    public static void alterarProprietario(Lista lista){
        Scanner leitura = new Scanner(System.in);
        String nome;
        Proprietario p = new Proprietario();
        System.out.println("Qual o nome: ");
        nome = leitura.nextLine();
        p.setNome(nome);
        lista.alterarProprietario(p);
    }
    public static void eliminarProprietario(Lista lista){
        Scanner leitura = new Scanner(System.in);
        String nome;
        Proprietario p = new Proprietario();
        System.out.println("Qual o nome: ");
        nome = leitura.nextLine();
        p.setNome(nome);
        lista.eliminarProprietario(p);
    }
    public static void loginProprietario(Lista lista){
        String nome,pw;
        Proprietario p = new Proprietario();
        Scanner leitura = new Scanner (System.in);
        System.out.println("Qual o nome?");
        nome = leitura.next();
        System.out.println("Password: ");
        pw = leitura.next();
        p.setNome(nome);
        p.setPassword(pw);
        if( lista.loginProprietario(p) != -1){
            menuProprietarioLvl1(lista,p);
        }

         
    }
    
    // Métodos para Hospedes
    public static void loginHospede(Lista lista){
        String nome,pw;
        Hospede h = new Hospede();
        Scanner leitura = new Scanner (System.in);
        System.out.println("Qual o nome?");
        nome = leitura.next();
        System.out.println("Password: ");
        pw = leitura.next();
        h.setNome(nome);
        h.setPassword(pw);
        if( lista.loginHospede(h) != -1){
            menuAlojamento(lista,h);
        }
    }
    public static void adicionarHospede(Lista lista){
        Scanner leitura = new Scanner(System.in);
        String nome,pw, morada, email, contacto;
        int nif, cc;
        Hospede h = new Hospede();
        System.out.println("Qual o nome: ");
        nome = leitura.nextLine();
        System.out.println("Password: ");        
        pw = leitura.nextLine();            
        System.out.println("Morada: ");
        morada = leitura.nextLine();       
        System.out.println("Email: ");
        email = leitura.nextLine();       
        System.out.println("Contacto: ");
        contacto = leitura.nextLine();
        System.out.println("NIF: ");
        nif = leitura.nextInt();
        System.out.println("Numero CC: ");
        cc = leitura.nextInt();
         h.setNome(nome);
         h.setPassword(pw);
         h.setMorada(morada);
         h.setContacto(contacto);
         h.setEmail(email);
         h.setCartaocid(cc);
         h.setNif(nif);
         lista.adicionarHospede(h);
        
        
    }
    public static void pesquisarHospede(Lista lista){
        Scanner leitura = new Scanner(System.in);
        String nome;
        Hospede h = new Hospede();
        System.out.println("Qual o nome: ");
        nome = leitura.nextLine();
        h.setNome(nome);
        lista.pesquisarHospede(h);
    }
    public static void alterarHospede(Lista lista){
        Scanner leitura = new Scanner(System.in);
        String nome;
        Hospede h = new Hospede();
        System.out.println("Qual o nome: ");
        nome = leitura.nextLine();
        h.setNome(nome);
        lista.alterarHospede(h);
    }
    public static void eliminarHospede(Lista lista){
        Scanner leitura = new Scanner(System.in);
        String nome;
        Hospede h = new Hospede();
        System.out.println("Qual o nome: ");
        nome = leitura.nextLine();
        h.setNome(nome);
        lista.eliminarHospede(h);
    }
    
    // Métodos para Alojamentos
    public static void adicionarAlojamento(Lista lista, Proprietario p){
        Scanner leitura = new Scanner(System.in);
        Alojamento aloj = new Alojamento();
        String nome, localizacao, servicos, tipologia;
        int preco = 0;
        Boolean disponiblidade = true;
        
        System.out.println("Qual o nome da Propriedade: ");
        nome = leitura.nextLine();
        
        System.out.println("Localização da Propriedade: ");
        localizacao = leitura.nextLine();
        
        System.out.println("Que principal serviços oferece?: ");
        servicos = leitura.nextLine();
        
        System.out.println("Tipologia da habitação: ");
        tipologia = leitura.nextLine();
        
        System.out.println("Preço por noite: ");
        preco = leitura.nextInt();
        
        aloj.setProprietario(lista.obterDadosProprietario(p));
        aloj.setNome(nome);        
        aloj.setLocalizacao(localizacao);
        aloj.setServicos(servicos);
        aloj.setTipologia(tipologia);
        aloj.setDisponiblidade(true);
        aloj.setPreco(preco);
        
                
        lista.adicionarAlojamento(aloj);
        gravar(lista);
                
    }
    public static void pesquisarAlojamento(Lista lista,Proprietario p){
        Scanner leitura = new Scanner(System.in);
        String nome;
        Alojamento a = new Alojamento();
        System.out.println("Qual o nome: ");
        nome = leitura.nextLine();
        a.setNome(nome);
        a.setProprietario(p);
        lista.pesquisarAlojamento(a);
    }
    public static void alterarAlojamento(Lista lista, Proprietario p){
        Scanner leitura = new Scanner(System.in);
        String nome,nomeprop;
        Alojamento a = new Alojamento();
        System.out.println("Qual o nome do alojamento: ");
        nome = leitura.nextLine();
        a.setNome(nome);
        a.setProprietario(p);
        lista.mostrarAlojamentos(p);
        lista.alterarAlojamento(a);
    }
    public static void eliminarAlojamento(Lista lista,Proprietario p){
        Scanner leitura = new Scanner(System.in);
        String nome;
        Alojamento a = new Alojamento();
        System.out.println("\n Qual o nome do alojamento: ");
        nome = leitura.nextLine();
        a.setNome(nome);
        a.setProprietario(p);
        lista.eliminarAlojamento(a);
    }
    
    // Métodos para Reservas
    public static void adicionarReserva(Lista lista, Hospede h){
        Scanner leitura = new Scanner(System.in);
        String nomealoj = "";
        int preco = 0 ;
        int dias;
        Reserva r = new Reserva();
        Alojamento aloj = new Alojamento();
        Proprietario p = new Proprietario();
        System.out.println("Qual o alojamento que pretende Reservar?:");
        nomealoj = leitura.nextLine();        
        System.out.println("Quantos dias ira querer reservar: ");
        dias = leitura.nextInt();    
        r.setHospede(lista.obterDadosHospede(h));
        aloj = lista.obterDadosAloj(nomealoj);
        if( aloj.getProprietario() != null ){
            r.setAlojamento(aloj);
            r.setDias(dias);
            lista.adicionarReserva(r);            
        }else{
            System.out.println("Não existe esse alojamento!");
        }
        
    }  
    public static void cancelarReserva(Lista lista,Hospede h){
        Scanner leitura = new Scanner(System.in);
        String nomealoj = "";
        int preco = 0 ;
        int dias;
        Reserva r = new Reserva();
        Alojamento aloj = new Alojamento();
        Proprietario p = new Proprietario();
        System.out.println("Qual o alojamento?:");
        nomealoj = leitura.nextLine();         
        lista.cancelarReserva(nomealoj, h);
    }
    //le informação da agenda num ficheiro
    public static Lista ler() {
        
        Lista listas = new Lista();        
        
        Armazenar grava = new Armazenar();
        listas=grava.lerDados("listas.arq");
        return listas;
    }
    
    //grava informação da agenda num ficheiro
      public static void gravar(Lista listas)
    {
        
        Armazenar grava = new Armazenar();
       
         grava.salvarDados(listas);
        
        
    }
}