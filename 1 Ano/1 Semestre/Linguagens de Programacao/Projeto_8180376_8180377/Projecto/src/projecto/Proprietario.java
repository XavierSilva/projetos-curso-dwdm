
package projecto;

import java.io.Serializable;

public class Proprietario extends Pessoa implements Serializable {
    // Propriedades
    private int rentablizacao = 0;
    private int totalReservas = 0;
    //Contrutores
    public Proprietario() {
    }

    public Proprietario(String nome, String morada, String email, String contacto,String password) {
        super(nome, morada, email, contacto,password);
    }
    // Getters e Setters
    public int getTotalReservas() {
        return totalReservas;
    }

    public void setTotalReservas(int totalReservas) {
        this.totalReservas = totalReservas;
    }
    
    public int getRentablizacao() {
        return rentablizacao;
    }
    public void setRentablizacao(int rentablizacao) {
        this.rentablizacao = rentablizacao;
    }
    // toString
    @Override
    public String toString() {
        return "Proprietario{\n Nome: " + super.getNome() +  ",\n Morada: " + super.getMorada() + ", \n Email: " + super.getEmail() + ",  \n Contacto: " + super.getContacto() +", \n Rentablizacao=" + rentablizacao +", \n Total de Reservas=" + totalReservas+ '}';
    }
    
    
}
