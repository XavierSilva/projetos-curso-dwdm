
package projecto;

import java.io.Serializable;


public class Reserva implements Serializable{
    // Propriedades
    private Hospede hospede;
    private Alojamento alojamento;
    private int Dias;
    // 1 para ativa  0 para finalizada
    private int estado = 1;

    // Getters e Setters
    public Hospede getHospede() {
        return hospede;
    }

    public void setHospede(Hospede hospede) {
        this.hospede = hospede;
    }

    public Alojamento getAlojamento() {
        return alojamento;
    }

    public void setAlojamento(Alojamento alojamento) {
        this.alojamento = alojamento;
    }

    public int getDias() {
        return Dias;
    }

    public void setDias(int Dias) {
        this.Dias = Dias;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
    // Contrutores
    public Reserva() {

    }

    public Reserva(Hospede hospede, Alojamento alojamento, int Dias) {

        this.hospede = hospede;
        this.alojamento = alojamento;
        this.Dias = Dias;
        this.estado = 1;
    }
    // toString
    @Override
    public String toString() {
        String est ="";    
        if(this.estado == 1){
            est = "Ativa";
        }else{
            est = "Finalizada";
        }
    
        return "Reserva{ \n Estado:"+  est + "\n Hospede=" + hospede.getNome() + "\n Alojamento=" + alojamento.getNome()+ " \n Dias=" + Dias + "\n }";
    }
    
    
   
}
