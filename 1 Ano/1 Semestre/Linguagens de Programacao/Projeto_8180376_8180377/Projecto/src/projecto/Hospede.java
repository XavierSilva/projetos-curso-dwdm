package projecto;

import java.io.Serializable;

public class Hospede extends Pessoa implements Serializable{
    //Propriedades
    private int nif;
    private int cartaocid;
    // Contrutores
    public Hospede(){
        
    }
    public Hospede(int nif, int cartaocid) {
        this.nif = nif;
        this.cartaocid = cartaocid;
    }

    public Hospede(int nif, int cartaocid, String nome, String morada, String email, String contacto,String password) {
        super(nome, morada, email, contacto,password);
        this.nif = nif;
        this.cartaocid = cartaocid;
    }
    // Getters e Setters
    public int getNif() {
        return nif;
    }

    public void setNif(int nif) {
        this.nif = nif;
    }

    public int getCartaocid() {
        return cartaocid;
    }

    public void setCartaocid(int cartaocid) {
        this.cartaocid = cartaocid;
    }
    // toString
    @Override
    public String toString() {
        return "Hospede{\n Nome: " + super.getNome() +  ",\n Morada: " + super.getMorada() + ", \n Email: " + super.getEmail() + ",  \n Contacto: " + super.getContacto() + "\n nif=" + nif + ",\n cartaocid= " + cartaocid + '}';
    }
    
    
}
