<div class="modal" id="AdicionarFilme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title">Adicionar Filme </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/sql.php" class="form" role="form" enctype="multipart/form-data" autocomplete="off" id="formLogin" novalidate="" method="POST">
                    <input type="hidden" name="metodo" value="inserirFilme">
                    <div class="form-group">
                        <label>Titulo</label>
                        <input type="text" class="form-control form-control-lg" name="titulo" required="">
                    </div>
                    <div class="form-group">
                        <label>Género</label>
                        <br>
                        <input type="radio" name="genero" value="acao" checked> Ação<br>
                        <input type="radio" name="genero" value="animacao"> Animação<br>
                        <input type="radio" name="genero" value="terror"> Terror<br><br>
                    </div>
                    <div class="form-group">
                        <label>Disponiblidade</label>
                        <br>
                        <input type="radio" name="estado" value="Disponivel" checked> Disponivel<br>
                        <input type="radio" name="estado" value="Brevemente"> Brevemente
                    </div>
                    <div class="form-group">
                        <label>Sinopse</label>
                        <textarea name="sinopse" rows="4" cols="30">Aqui coloque a sinopse...</textarea>
                    </div>
                    <div class="form-group">
                        <label>Imagem</label>
                        <input type="file" name="image" id="image" />

                    </div>
                    <div class="form-group py-4">
                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        <button type="submit" name="submit" value="UPLOAD" class="btn btn-success btn-lg float-right" id="btnLogin">Adicionar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="EditarFilme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title">Editar Filme </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/sql.php" class="form" role="form" enctype="multipart/form-data" autocomplete="off" id="formLogin" novalidate="" method="POST">
                    <input type="hidden" name="metodo" value="editarFilme">
                    <div class="form-group">
                        <label>Titulo</label>
                        <input type="text" class="form-control form-control-lg" required="" name="titulo">
                    </div>
                    <div class="form-group">
                        <label>Género</label>
                        <br>
                        <input type="radio" name="genero" value="acao" checked> Ação<br>
                        <input type="radio" name="genero" value="animacao"> Animação<br>
                        <input type="radio" name="genero" value="terror"> Terror<br><br>
                    </div>
                    <div class="form-group">
                        <label>Sinopse</label>
                        <textarea name="sinopse" rows="4" cols="30">Aqui coloque a sinopse...</textarea>
                    </div>
                    <div class="form-group">
                        <input type="file" id="image" name="image" />
                    </div>
                    <div class="form-group py-4">
                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        <button type="submit" name="submit" value="UPLOAD" class="btn btn-success btn-lg float-right" id="btnLogin">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="EliminarFilme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title">Eliminar Filme </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/sql.php" class="form" role="form" autocomplete="off" id="formLogin" novalidate="" method="POST">
                    <input type="hidden" name="metodo" value="apagarFilme">
                    <div class="form-group">
                        <label>Titulo</label>
                        <input type="text" class="form-control form-control-lg" name="titulo">
                    </div>

                    <div class="form-group py-4">
                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        <button type="submit" class="btn btn-success btn-lg float-right" id="btnLogin">Eliminar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>