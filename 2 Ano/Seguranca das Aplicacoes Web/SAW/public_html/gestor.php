<!DOCTYPE HTML>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<?php
include 'conection.php';
error_reporting(0);
session_start();
include 'modalsGestor.php';
$tipo = $_SESSION['tipoUser'];
if ($tipo === 'Normal') {
    echo "<script language='javascript' type='text/javascript'>alert('Não tem permissoes para aceder a esta pagina!');window.location.href='index.php'</script>";
}
?>

<html>

<head>
    <title>SAW</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/main.css" />
</head>

<body>
    <div class="page-wrap">

        <!-- Nav -->
        <?php include 'nav.php'; ?>

        <!-- Main -->
        <section id="main">
            <h1 class="display-1">Gestão Filmes</h1>
            <div class="butoe_gestao">

                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#AdicionarFilme" style="margin-left: 35px">Adicionar </button>
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#EditarFilme" style="margin-left: 5px">Editar</button>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#EliminarFilme" style="margin-left: 5px">Eliminar</button>
                <a href="gestorUsers.php"><button type="button" style="margin-left:250px" class="btn btn-primary">Gestão Utilizadores</button></a>
                <a href="gestorReservas.php"><button type="button" class="btn btn-primary">Gestão Reservas</button></a>
            </div>
            <br>
            <br>
            <br>
            <div style="margin-left: 35px">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Nome </th>
                            <th>Género </th>
                            <th>Disponibilidade </th>
                            <th>Imagem </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $stmt = $conn->prepare('SELECT * FROM Filmes');

                        $stmt->execute();
                        $result = $stmt->get_result();
                        if ($result->num_rows > 0) {
                            while ($row = $result->fetch_assoc()) {

                        ?>
                                <tr>
                                    <td><?php echo $row['titulo']; ?></td>
                                    <td><?php echo $row['genero']; ?></td>
                                    <td><?php echo $row['estado']; ?></td>
                                    <td><?php echo '<img src="data:image/jpeg;base64,' . base64_encode($row['image']) . '" height="60" width="45"/>'; ?></td>
                                </tr>
                        <?php
                            }
                        } else {
                            echo "Não existem filmes";
                        }
                        ?>
                    </tbody>
                </table>
            </div>





            <!-- Gallery -->





            <!-- Footer -->
            <?php include 'footer.php'; ?>
        </section>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


</body>

</html>