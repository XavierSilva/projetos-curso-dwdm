<?php   
include 'logs.php';
session_start(); //to ensure you are using same session
$email = $_SESSION['email'];
wh_log("Logout  : " . $email);
session_destroy(); //destroy the session
header("location:/index.php"); //to redirect back to "index.php" after logging out
exit();
?>