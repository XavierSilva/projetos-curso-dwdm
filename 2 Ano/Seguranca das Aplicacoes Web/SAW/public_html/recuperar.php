<!DOCTYPE HTML>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<?php error_reporting(0);
session_start();
include 'conection.php';
include 'modals.php';
?>




<html>

<head>
    <title>SAW</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/main.css" />
</head>

<body>
    <div class="page-wrap">

        <!-- Nav -->
        <?php include 'nav.php'; ?>

        <!-- Main -->
        <section id="main">
            <h1 style="margin:90px">Recuperar Conta</h1>

            <form action="" method="POST" style="margin:90px">
                <div class="form-group">
                    <label for="exampleInputEmail1">Password<small>nova</small></label>
                    <input type="password" style="max-width: 350px" name="password1" class="form-control form-control-lg" id="pwd1" required="" minlength="8" autocomplete="">

                </div>
                <div class=" form-group">
                    <label for="exampleInputPassword1">Repita Password<small>nova</small></label>
                    <input type="password" style="max-width: 350px" name="password2" class="form-control form-control-lg" id="pwd1" required="" minlength="8" autocomplete="">
                </div>
                <button type="submit" id="sub" name="sub" value="sub">Recuperar</button>

                <?php
                if (isset($_POST["sub"])) {


                    $pass1 = $_POST['password1'];
                    $pass2 = $_POST['password2'];

                    if (isset($_GET['email']) && !empty($_GET['email'])) {
                        // Verify data
                        $email = ($_GET['email']);
                        if ($pass1 === $pass2) {

                            $uppercase = preg_match('@[A-Z]@', $pass1);
                            $lowercase = preg_match('@[a-z]@', $pass1);
                            $number    = preg_match('@[0-9]@', $pass1);
                            $specialChars = preg_match('@[^\w]@', $pass1);
                            if (!$uppercase || !$lowercase || !$number || !$specialChars || strlen($pass1) < 8) {
                                echo "<script language='javascript' type='text/javascript'>alert('Passwords deve conter letras M/m  Numeros e Caracteres especiais!');</script>";
                            } else {

                                $pw_hash = password_hash($pass1, PASSWORD_DEFAULT);
                                $stmt2 = $conn->prepare('UPDATE  Utilizadores  SET password=? WHERE email=?');
                                $stmt2->bind_param('ss', $pw_hash, $email);
                                $stmt2->execute();

                                if ($stmt2->affected_rows === 1) {
                                    $stmt2->close();
                                    echo "<script language='javascript' type='text/javascript'>alert('Password alterada!');window.location.href='index.php'</script>";
                                } else {
                                    echo "<script language='javascript' type='text/javascript'>alert('Erro na recuperaçãod e conta!');'</script>";
                                    $stmt2->close();
                                }
                                $stmt1->close();
                            }
                        } else {
                            echo "<script language='javascript' type='text/javascript'>alert('Passwords Diferentes!');</script>";
                        }
                    } else {
                    }
                }

                ?>
            </form>
            <!-- Contact -->
            <h1 style="height:250px"></h1>

            <!-- Footer -->
            <?php include 'footer.php'; ?>
        </section>

    </div>
</body>


</html>



<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>