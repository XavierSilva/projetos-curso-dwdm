<?php
include 'conection.php';
include 'logs.php';

// começar ou retomar uma sessão
session_start();

// se vier um pedido para login
if (!empty($_POST)) {

	$email = $conn->real_escape_string($_POST['email']);
	$password = $conn->real_escape_string($_POST['password']);
	$stmt = $conn->prepare("SELECT * FROM Utilizadores WHERE email = ?");
	$stmt->bind_param("s", $email);
	$stmt->execute();
	$result = $stmt->get_result();
	if ($result->num_rows === 1) {
		$row = $result->fetch_assoc();
		$ativo = $row['ativo'];
		if ($ativo == 0) {
			echo "<script language='javascript' type='text/javascript'>alert('Ainda nao ativou a sua conta!');window.location.href='index.php'</script>";
		}
		if ($row['tipoUser'] === "Desativado") {
			echo "<script language='javascript' type='text/javascript'>alert('A sua conta está Desativada/ Banida. Contacte o suporte');window.location.href='index.php'</script>";
		} else {
			if (password_verify($password, $row['password'])) {
				$_SESSION['email'] = $email;
				$_SESSION['tipoUser'] = $row['tipoUser'];
				$hour = time() + 3600 * 24 * 30;
				setcookie($email, $login, $hour);
				setcookie($row['password'], $password, $hour);
				wh_log("Login efetuado por :" . $email);
				header('location:profile.php');
			} else {
				wh_log("Erro de login, password invalida por :" . $email);
				echo "<script language='javascript' type='text/javascript'>alert('Email ou Password Invalido!');window.location.href='index.php'</script>";
				session_unset();
			}
		}
	} else {
		wh_log("Erro de login, email invalido : " . $email);
		echo "<script language='javascript' type='text/javascript'>alert('Não está registado no site!');window.location.href='index.php'</script>";
		session_unset();
	}

	$stmt->close();
}
