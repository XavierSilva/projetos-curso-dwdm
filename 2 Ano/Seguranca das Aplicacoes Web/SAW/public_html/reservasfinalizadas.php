<!DOCTYPE HTML>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<?php
include 'conection.php';
error_reporting(0);
session_start();
$conta = $_SESSION['email'];
include 'modalsGestor.php'; ?>

<html>

<head>
    <title>SAW</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/main.css" />
</head>

<body>
    <div class="page-wrap">

        <!-- Nav -->
        <?php include 'nav.php'; ?>

        <!-- Main -->
        <section id="main">
            <h1 class="display-1">Histórico Reservas</h1>

            <br>
            <br>
            <br>
            <div style="margin-left: 35px">
                <h4>Histórico</h4>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Titulo</th>
                            <th>Género</th>
                            <th>Imagem</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php


                        $estado = "Devolvido";

                        $stmt = $conn->prepare('SELECT * FROM filmes f, reservas r WHERE r.idUser = ? AND f.titulo=r.idFilme AND r.estado=?');
                        $stmt->bind_param('ss', $conta, $estado);
                        $stmt->execute();
                        $result = $stmt->get_result();


                        if ($result->num_rows > 0) {

                            while ($row = $result->fetch_assoc()) {

                        ?>
                                <tr>
                                    <td><?php echo $row['titulo']; ?></td>
                                    <td><?php echo $row['genero']; ?></td>
                                    <td><?php echo '<img src="data:image/jpeg;base64,' . base64_encode($row['image']) . '" height="60" width="45"/>'; ?></td>
                                </tr>
                        <?php
                            }
                        } else {
                            echo "Não tem reservas!";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col">

                    </div>
                    <div class="col-6">

                    </div>
                    <div class="col">
                        <a href="myreservas.php">
                            <button type="button">Voltar</button></a>
                    </div>
                </div>





                <!-- Gallery -->





                <!-- Footer -->
                <?php include 'footer.php'; ?>
        </section>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


</body>

</html>