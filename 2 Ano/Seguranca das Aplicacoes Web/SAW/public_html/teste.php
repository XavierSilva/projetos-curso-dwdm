<link rel="stylesheet" href="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/css/bootstrap.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!-- Modal -->
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<!--Content-->
		<div class="modal-content form-elegant">
			<!--Header-->
			<div class="modal-header text-center">
				<h3 class="modal-title w-100 dark-grey-text font-weight-bold my-3" id="myModalLabel"><strong>Sign
						in</strong></h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<!--Body-->

			<div class="modal-body mx-4">
				<!--Body-->
				<form action="/login.php" method="post">
					<div class="md-form mb-5">
						<input type="email" name="email" id="Form-email1" class="form-control validate">
						<label data-error="wrong" data-success="right" for="Form-email1">Your email</label>
					</div>

					<div class="md-form pb-3">
						<input type="password" name="password" id="Form-pass1" class="form-control validate">
						<label data-error="wrong" data-success="right" for="Form-pass1">Your password</label>
						<p class="font-small blue-text d-flex justify-content-end">Forgot Password?</p>
					</div>

					<div class="modal-footer d-flex justify-content-center">
					<button class="btn btn-deep-orange">Login</button>
				</div>
				</form>
			</div>
			<!--Footer-->
			<div class="modal-footer mx-5 pt-3 mb-1">
				<p class="font-small grey-text d-flex justify-content-end">Not a member? <a href="#" class="blue-text ml-1" data-toggle="modal" data-target="#modalRegisterForm">
						Sign Up</a></p>
			</div>

		</div>
		<!--/.Content-->
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header text-center">
				<h4 class="modal-title w-100 font-weight-bold">Sign up</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="/sql.php" method="post">
				<div class="modal-body mx-3">
					<div class="md-form mb-5">
						<i class="fas fa-user prefix grey-text"></i>
						<input type="text" name="nome" id="orangeForm-name" class="form-control validate">
						<label data-error="wrong" data-success="right" for="orangeForm-name">Nome</label>
					</div>
					<div class="md-form mb-5">
						<i class="fas fa-envelope prefix grey-text"></i>
						<input type="email" name="email" id="orangeForm-email" class="form-control validate">
						<label data-error="wrong" data-success="right" for="orangeForm-email">Email</label>
					</div>

					<div class="md-form mb-4">
						<i class="fas fa-lock prefix grey-text"></i>
						<input type="password" name="password" id="orangeForm-pass" class="form-control validate">
						<label data-error="wrong" data-success="right" for="orangeForm-pass">Password</label>
					</div>

				</div>
				<div class="modal-footer d-flex justify-content-center">
					<button class="btn btn-deep-orange">Registar</button>
				</div>
			</form>
		</div>
	</div>
</div>