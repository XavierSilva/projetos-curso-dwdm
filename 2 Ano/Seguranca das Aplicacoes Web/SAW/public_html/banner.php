<!-- Banner -->

<section id="banner">
				<div class="inner">
					<h1>Clube de Video</h1>
					<p>Alugue aqui os seus filmes favoritos!</p>
					<ul class="actions">
						<li><a href="#galleries" class="button alt scrolly big">Continuar</a></li>
					</ul>
				</div>
</section>