<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<!-- Modal -->
<div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">REGISTO</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/sql.php" id="formLogin" method="POST">
                    <input type="hidden" name="metodo" value="inserirUser">
                    <div class="form-group">
                        <div class="form-group">
                            <label>Nome</label>
                            <input type="text" class="form-control form-control-lg" required="" name="nome">
                            <div class="invalid-feedback">Escreva o seu nome</div>
                        </div>
                        <div class="form-group">
                            <label>Morada</label>
                            <input type="text" class="form-control form-control-lg" required="" name="morada">

                        </div>
                        <div class="form-group">
                            <label>Idade</label>
                            <input type="number" class="form-control form-control-lg" required="" name="idade">

                        </div>
                        <hr style="background: #00000061">
                        <label>Email</label>
                        <input type="email" name="email" id="orangeForm-email" class="form-control validate" required="">
                        <div class="invalid-feedback">Oops, you missed this one.</div>

                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control form-control-lg" id="pwd1" required="" minlength="8" autocomplete="new-password">
                        <div class="invalid-feedback">Enter your password too!</div>
                    </div>

                    <div class="form-group py-4">
                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        <button type="submit" class="btn btn-success btn-lg float-right">Registar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="modalLoginForm" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">LOGIN</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/login.php" id="formLogin" method="POST">
                    <div class="form-group">
                        <label for="uname1">Email</label>
                        <input type="email" name="email" id="orangeForm-email" class="form-control validate" required="">
                        <div class="invalid-feedback">Oops, you missed this one.</div>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control form-control-lg" id="pwd1" required="" minlength="8" autocomplete="  current-password">
                        <div class="invalid-feedback">Enter your password too!</div>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="rememberMe">
                        <label class="custom-control-label" for="rememberMe">Remember</label>
                        <a href="" class="float-right" data-dismiss="modal" data-toggle="modal" data-target="#modalRegisterForm">Novo Utilizador?</a>

                    </div>
                    <div class="custom-control custom-checkbox">
                        <a href="" class="float-right" data-dismiss="modal" data-toggle="modal" data-target="#modalRecuperacao"><small>Esqueceu password?</small> ?</a>
                    </div>
                    <div class="form-group py-4">
                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        <button type="submit" class="btn btn-success btn-lg float-right" id="btnLogin">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="modalReserva" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Reservar Filme</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/sql.php" id="formReserva" method="POST">
                    <input type="hidden" name="metodo" value="inserirReserva">
                    <input type="hidden" name="conta" value="<?php echo $conta ?>">
                    <div class="form-group">
                        <label for="uname1">Titulo do filme :</label>
                        <input type="text" name="titulo" id="orangeForm-email" class="form-control validate" required="">
                        <div class="invalid-feedback">Oops, you missed this one.</div>
                    </div>

                    <div class="form-group py-4">

                        <button type="submit" class="btn btn-success btn-lg float-right" id="btnReserva">Efetuar Reserva</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="modalDevolver" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Devolução de Filme</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/sql.php" id="formReserva" method="POST">
                    <input type="hidden" name="metodo" value="devolverReserva">
                    <input type="hidden" name="conta" value="<?php echo $conta ?>">
                    <div class="form-group">
                        <label for="uname1">Titulo do filme :</label>
                        <input type="text" name="titulo" id="orangeForm-email" class="form-control validate" required="">
                        <div class="invalid-feedback">Oops, you missed this one.</div>
                    </div>

                    <div class="form-group py-4">

                        <button type="submit" class="btn btn-success btn-lg float-right" id="btnDevolver">Devolver Filme</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php

use function Composer\Autoload\includeFile;

include 'conection.php';
//include 'logs.php';
$stmt = $conn->prepare('SELECT * FROM Filmes');

$stmt->execute();
$result = $stmt->get_result();

$conta = $_SESSION['email'];

if ($result->num_rows > 0) {



    while ($row = mysqli_fetch_array($result)) {
?>
        <div id="modalFilme<?php echo $row['titulo'] ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <input type="hidden" name="hiddenValue" id="hiddenValue" value="" />
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">

                    <div class="modal-body">

                        <div class="row">
                            <div class="col-lg-5">

                                <div class="carousel-item active">
                                    <img <?php echo ' src="data:image/jpeg;base64,' . base64_encode($row['image']) . '"'; ?> />
                                </div>

                            </div>

                            <!--/.Carousel Wrapper-->

                            <div class="col-lg-7">
                                <h2 class="h2-responsive product-name">
                                    <strong style="text-align:center"> <?php echo $row['titulo'] ?> </strong>
                                </h2>
                                <hr style="border-color:black">
                                <h4>
                                    <span class="green-text">
                                        <strong>Sinopse</strong>
                                    </span><br>
                                    <small style="font-size: 58%;">
                                        <?php echo $row['sinopse'] ?> </small>
                                    <p>Género</p>

                                    <small>
                                        <p><?php echo $row['genero'] ?></p>
                                    </small>

                                    <br>

                                </h4>
                                <form action="/sql.php" id="formReserva" method="POST">
                                    <input type="hidden" name="metodo" value="inserirReserva">
                                    <input type="hidden" name="conta" value="<?php echo $conta ?>">
                                    <input type="hidden" name="titulo" value="<?php echo $row['titulo'] ?>">
                                    <div class="text-center">

                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                        <?php
                                        if ($conta != null) {

                                            if ($row['estado'] == "Disponivel") {
                                        ?>
                                                <button class="btn btn-primary">Alugar
                                                <?php
                                            } elseif ($row['estado'] == "Brevemente") {
                                                ?>
                                                    <button disabled="false" class="btn btn-danger">Brevemente
                                                    <?PHP
                                                } else {
                                                    ?>
                                                        <button disabled="false" class="btn btn-danger">Alugado
                                                    <?PHP
                                                }
                                            }

                                                    ?>




                                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.Add to Cart -->
                    </div>
                </div>
            </div>
        </div>
<?php
    }
} else {
    echo "0 results";
}

?>

<div class="modal" id="EditarUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Editar Utilizador</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/sql.php" id="formLogin" method="POST">
                    <input type="hidden" name="metodo" value="EditarUser">
                    <div class="form-group">
                        <div class="form-group">
                            <label>Nome</label>
                            <input type="text" class="form-control form-control-lg" required="" name="nome">
                            <div class="invalid-feedback">Escreva o seu nome</div>
                        </div>
                        <div class="form-group">
                            <label>Morada</label>
                            <input type="text" class="form-control form-control-lg" required="" name="morada">

                        </div>
                        <div class="form-group">
                            <label>Idade</label>
                            <input type="number" class="form-control form-control-lg" required="" name="idade">

                        </div>

                    </div>
                    <div class="form-group">
                        <label>Password<small>(Introduza a atual se não pretende alterar)</small></label>
                        <input type="password" name="password" class="form-control form-control-lg" id="pwd1" required="" minlength="8" autocomplete="new-password">
                        <div class="invalid-feedback">Enter your password too!</div>
                    </div>

                    <div class="form-group py-4">
                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        <button type="submit" class="btn btn-success btn-lg float-right">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="eliminarUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Eliminar Conta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/sql.php" id="formLogin" method="POST">
                    <input type="hidden" name="metodo" value="eliminarUser">
                    <input type="hidden" name="email" value=<?php $conta ?>>

                    <div class="form-group">
                        <label>Tem a certeza que deseja eliminar a sua conta?</label>
                    </div>

                    <div class="form-group py-4">
                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Não</button>
                        <button type="submit" class="btn btn-success btn-lg float-right">Sim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal" id="editarPrivilegios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title">Editar Privilegios</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/sql.php" class="form" role="form" enctype="multipart/form-data" autocomplete="off" id="formLogin" novalidate="" method="POST">
                    <input type="hidden" name="metodo" value="editarprivilegios">
                    <div class="form-group">
                        <label for="uname1">Email</label>
                        <input type="email" name="email" id="orangeForm-email" class="form-control validate" required="">
                        <div class="invalid-feedback">Oops, you missed this one.</div>
                    </div>
                    <div class="form-group">
                        <label>Tipo Utilizador</label>
                        <br>
                        <input type="radio" name="tipoUser" value="Normal" checked> Cliente<br>
                        <input type="radio" name="tipoUser" value="Admin">Administrador<br>

                    </div>
                    <div class="form-group py-4">
                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        <button type="submit" name="submit" value="UPLOAD" class="btn btn-success btn-lg float-right" id="btnLogin">Alterar</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal" id="banir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title">Banir Cliente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/sql.php" class="form" role="form" enctype="multipart/form-data" autocomplete="off" id="formLogin" novalidate="" method="POST">
                    <input type="hidden" name="metodo" value="banir">
                    <div class="form-group">
                        <label for="uname1">Email</label>
                        <input type="email" name="email" id="orangeForm-email" class="form-control validate" required="">
                        <div class="invalid-feedback">Oops, you missed this one.</div>
                    </div>
                    <div class="form-group py-4">
                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        <button type="submit" name="submit" value="UPLOAD" class="btn btn-danger btn-lg float-right " id="btnLogin">Banir</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal" id="eliminarReserva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title">Eliminar Reserva </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/sql.php" class="form" role="form" enctype="multipart/form-data" autocomplete="off" id="formLogin" novalidate="" method="POST">
                    <input type="hidden" name="metodo" value="eliminarReserva">
                    <div class="form-group">
                        <label for="uname1">Titulo</label>
                        <input type="text" name="titulo" id="orangeForm-email" class="form-control validate" required="">
                        <div class="invalid-feedback">Oops, you missed this one.</div>
                    </div>
                    <div class="form-group py-4">
                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        <button type="submit" name="submit" value="UPLOAD" class="btn btn-danger btn-lg float-right " id="btnLogin">Eliminar</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal" id="modalRecuperacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title">Recuperar conta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/sql.php" class="form" role="form" enctype="multipart/form-data" autocomplete="off" id="formLogin" novalidate="" method="POST">
                    <input type="hidden" name="metodo" value="recuperar">
                    <div class="form-group">
                        <label for="uname1">Email</label>
                        <input type="email" name="email" id="orangeForm-email" class="form-control validate" required="">
                        <div class="invalid-feedback">Oops, you missed this one.</div>
                    </div>

                    <div class="form-group py-4">
                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                        <button type="submit" name="submit" value="UPLOAD" class="btn btn-success btn-lg float-right" id="btnLogin">Recuperar</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>