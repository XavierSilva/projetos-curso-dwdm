<!DOCTYPE HTML>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<?php error_reporting(0);
session_start();
$conta = $_SESSION['email'];
include 'sql.php';
include 'modals.php'; ?>
<html>

<head>
	<title>SAW</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="assets/css/main.css" />
</head>

<body>
	<div class="page-wrap">

		<!-- Nav -->
		<?php include 'nav.php'; ?>

		<!-- Main -->
		<section id="main">

			<!-- Gallery -->
			<section id="galleries">

				<!-- Photo Galleries -->
				<div class="gallery">

					<!-- Filters -->
					<header>
						<h1>Filmes</h1>
						<?php
						if ($conta != null) { ?>

							<ul class="tabs">
								<li><a href="#" data-toggle="modal" data-target="#modalReserva" data-tag="all" class="button active">Reservar Filme</a></li>

							</ul>
						<?php
						}
						?>



					</header>

					<div class="content">
						<?php
						include 'conection.php';
						mostrarFilmes($conn);
						?>
					</div>
			</section>



			<!-- Footer -->
			<?php include 'footer.php'; ?>
		</section>
	</div>

	<!-- Scripts -->

	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>