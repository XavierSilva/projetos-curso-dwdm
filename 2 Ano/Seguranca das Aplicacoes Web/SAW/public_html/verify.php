
<?php
error_reporting(0);
session_start();
include 'conection.php';
if (isset($_GET['email']) && !empty($_GET['email']) and isset($_GET['codValidacao']) && !empty($_GET['codValidacao'])) {
    // Verify data
    $email = ($_GET['email']);
    $codValidacao = ($_GET['codValidacao']);
    $ativo = 0;

    $stmt = $conn->prepare('SELECT * FROM utilizadores WHERE email= ? AND codValidacao=? AND ativo=?');
    $stmt->bind_param('sii', $email, $codValidacao, $ativo);
    $stmt->execute();
    $result = $stmt->get_result();


    if ($result->num_rows > 0) {
        $ativo = 1;
        $stmt = $conn->prepare('UPDATE utilizadores SET ativo= ? WHERE email = ?');
        $stmt->bind_param('is', $ativo, $email);

        $stmt->execute();


        if ($stmt->affected_rows === 1) {

            echo "<script language='javascript' type='text/javascript'>alert('CONTA ATIVADA COM SUCESSO!');window.location.href='index.php'</script>";
        }
    }
}


?>