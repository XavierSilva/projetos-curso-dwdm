<?php

use PHPMailer\PHPMailer\PHPMailer;

include 'conection.php';
include 'logs.php';

$metodo = $_POST['metodo'];
switch ($metodo) {
     case "inserirFilme":
          inserirFilme($conn);
          break;
     case "apagarFilme":
          apagarFilme($conn);
          break;
     case "editarFilme":
          editarFilme($conn);
          break;
     case "inserirUser":
          inserirUser($conn);
          break;
     case "EditarUser":
          editarUser($conn);
          break;
     case "eliminarUser":
          eliminarUser($conn);
          break;
     case "inserirReserva":
          inserirReserva($conn);
          break;
     case "devolverReserva":
          devolverReserva($conn);
          break;
     case "editarprivilegios":
          editarprivilegios($conn);
          break;
     case "eliminarReserva":
          eliminarReserva($conn);
          break;
     case "updateFoto":
          updateFoto($conn);
          break;
     case "banir":
          banir($conn);
          break;
}


function criarTabelaUsers($conn)
{
     $sql = "CREATE TABLE IF NOT EXISTS Utilizadores (
          email VARCHAR(30)  PRIMARY KEY,
          nome VARCHAR(30) NOT NULL,
          morada VARCHAR(90) NOT NULL,
          idade int(3) NOT NULL,
          password VARCHAR(90) NOT NULL,
          ativo int(1) NOT NULL,
          codValidacao int(4) NOT NULL,
          tipoUser VARCHAR(10) NOT NULL,
          image longblob)";

     if ($conn->query($sql) === TRUE) echo "Tabela Utilizadores criada com sucesso!!!";
     else echo "Erro na criação da tabela: " . $conn->error;
}

function criarTabelaFilmes($conn)
{

     $sql = "CREATE TABLE IF NOT EXISTS Filmes (
          titulo VARCHAR(30) NOT NULL PRIMARY KEY,
          genero VARCHAR(30) NOT NULL,
          estado VARCHAR(30) NOT NULL,
          sinopse VARCHAR(90) NOT NULL,
          image longblob NOT NULL)";
     if ($conn->query($sql) === TRUE) echo "Tabela Filmes criada com sucesso!!!";
     else echo "Erro na criação da tabela: " . $conn->error;
}

function criarTabelaReservas($conn)
{
     $sql = "CREATE TABLE IF NOT EXISTS Reservas(
          idReserva int NOT NULL  PRIMARY KEY AUTO_INCREMENT,
          idFilme VARCHAR(30) NOT NULL,
          idUser VARCHAR(30) NOT NULL,
          FOREIGN KEY (idFilme) REFERENCES Filmes (titulo),
          FOREIGN KEY (idUser) REFERENCES Utilizadores (email),
          estado VARCHAR(20) NOT NULL)";
     if ($conn->query($sql) === TRUE) echo "Tabela Reservas criada com sucesso!!!";
     else echo "Erro na criação da tabela: " . $conn->error;
}
function mostrarFilmes($conn)
{
     $stmt = $conn->prepare('SELECT * FROM Filmes');

     $stmt->execute();
     $result = $stmt->get_result();
     if ($result->num_rows > 0) {
?>

          <?php
          while ($row = mysqli_fetch_array($result)) {
               if ($row['estado'] != "Desativado") {


          ?>
                    <div class="media all people">


                         <a href="#" data-toggle="modal" data-target="#modalFilme<?php echo $row['titulo'] ?>">
                              <img <?php echo ' src="data:image/jpeg;base64,' . base64_encode($row['image']) . '"'; ?> />
                         </a>
                         <br>

                    </div>
<?php
               }
          }
     } else {
          echo "<h2>Não existem filme.</h2>";
     }
}
function inserirUser($conn)
{
     $cod_validacao = rand(1000, 9000);
     $nome = $conn->real_escape_string($_POST['nome']);
     $morada = $conn->real_escape_string($_POST['morada']);
     $idade =  ($_POST['idade']);
     $email = $conn->real_escape_string($_POST['email']);
     $password = $conn->real_escape_string($_POST['password']);
     $ativo = 0;
     $tipoUser = "Normal";
     $uppercase = preg_match('@[A-Z]@', $password);
     $lowercase = preg_match('@[a-z]@', $password);
     $number    = preg_match('@[0-9]@', $password);
     $number2    = preg_match('@[0-9]@', $idade);
     $specialChars = preg_match('@[^\w]@', $password);
     $img = null;

     if (!preg_match("/^[a-zA-Z ]*$/", $nome)) {

          echo "<script language='javascript' type='text/javascript'>alert('Só aceita letras no nome!');window.location.href='index.php'</script>";
     } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          echo "Email invalido!";
          echo "<script language='javascript' type='text/javascript'>alert('E-MAIL INVALIDO!');window.location.href='index.php'</script>";
     } elseif (!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
          echo 'Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.';
          echo "<script language='javascript' type='text/javascript'>alert('E-MAIL INVALIDO!');window.location.href='index.php'</script>";
     } elseif (!$number2 ||  $idade < 0) {
          echo "<script language='javascript' type='text/javascript'>alert('A idade deve ser um Numero e/ou Positivo!');window.location.href='index.php'</script>";
     } else {

          $pw_hash = password_hash($password, PASSWORD_DEFAULT);
          $stmt = $conn->prepare('INSERT INTO Utilizadores VALUES(?,?,?,?,?,?,?,?,?)');
          $stmt->bind_param('sssisiiss', $email, $nome, $morada, $idade, $pw_hash, $ativo, $cod_validacao, $tipoUser, $img);

          $stmt->execute();


          if ($stmt->affected_rows === 1) {
               //Import PHPMailer classes into the global namespace


               require './vendor/autoload.php';
               //Create a new PHPMailer instance
               $mail = new PHPMailer;
               //Charset for the message
               $mail->CharSet = 'utf-8';
               //Tell PHPMailer to use SMTP
               $mail->isSMTP();
               //Enable SMTP debugging
               $mail->SMTPDebug = 2; //0 = off (for production use); 1 = client messages; 2 = client and server messages
               //Set the hostname of the mail server
               $mail->Host = 'smtp.gmail.com';
               //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
               $mail->Port = 587;
               //Set the encryption system to use - ssl (deprecated) or tls
               $mail->SMTPSecure = 'tls';
               //Whether to use SMTP authentication
               $mail->SMTPAuth = true;
               //Username to use for SMTP authentication - use full email address for gmail
               $mail->Username = "sawestgipp@gmail.com";
               //Password to use for SMTP authentication
               $mail->Password = "SAW2020ipp";
               //Set who the message is to be sent from
               $mail->setFrom('seg.apli.web@gmail.com', 'Segurança das Aplicações Web');
               //Set who the message is to be sent to
               $mail->addAddress($email, '   ');
               //Set the subject line
               $mail->Subject = 'SAW.com';
               //allow html code in body message
               $mail->IsHTML(true);
               //Body text for the message
               $mail->Body = "
 
               Obrigado pelo seu registo
               A sua conta foi criada com sucesso!Dados de acesso:
               Username: '$nome'
               Password: '$password'          
                
               Clique no link para ativar a sua conta:
               http://saw.com/verify.php?email=$email&codValidacao=$cod_validacao";
               //aditional options to SMTP
               $mail->SMTPOptions = array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true));

               if (!$mail->send()) {
                    echo "Mailer Error: " . $mail->ErrorInfo;
               } else {
                    echo "Message sent!";
               }
               wh_log("Conta registada :" . $email);
               echo "<script language='javascript' type='text/javascript'>alert('Conta criada com Sucesso , Aceda ao seu email para ativar a conta. Efectue login!');window.location.href='index.php'</script>";
          } else {
               echo "<script language='javascript' type='text/javascript'>alert('Algo correu mal!');window.location.href='index.php'</script>";
          }
     }
     $stmt->close();
}
function eliminarUser($conn)
{
     session_start();
     $email = $_SESSION['email'];

     $stmt = $conn->prepare('SELECT r.idFilme, r.idReserva,r.idUser,r.estado FROM Reservas r, Filmes f WHERE r.idUser=? ORDER BY r.estado DESC');
     $stmt->bind_param("s", $email);
     $stmt->execute();
     $result = $stmt->get_result();
     if ($result->num_rows > 0) {
          $row = mysqli_fetch_array($result);
          //echo "<script language='javascript' type='text/javascript'>alert('Existe uma reserva desse filme Ativa!');window.location.href='gallery.php'</script>";
          if ($row['estado'] === "Devolvido") {
               $estado = "Desativado";
               $stmt2 = $conn->prepare("UPDATE Utilizadores SET tipoUser=? WHERE email=?");
               $stmt2->bind_param("ss", $estado, $email);
               $stmt2->execute();

               if ($stmt2->affected_rows === 1) {
                    session_destroy(); //destroy the session
                    wh_log("Conta desativa pelo cliente :" . $email);
                    echo "<script language='javascript' type='text/javascript'>alert('A sua conta foi eliminada!');window.location.href='index.php'</script>";
               } else {

                    echo "<script language='javascript' type='text/javascript'>alert('Algo correu mal!');window.location.href='index.php'</script>";
               }
               $stmt2->close();
          } else {
               echo "<script language='javascript' type='text/javascript'>alert('Existem Filmes reservados por si!');window.location.href='profile.php'</script>";
          }
     } else {
          $estado = "Desativado";
          $stmt2 = $conn->prepare('UPDATE Utilizadores SET tipoUser=? WHERE email=?');
          $stmt2->bind_param("ss", $estado, $email);
          $stmt2->execute();

          if ($stmt2->affected_rows === 1) {

               session_destroy(); //destroy the session
               wh_log("Conta desativa pelo cliente :" . $email);
               echo "<script language='javascript' type='text/javascript'>alert('A sua conta foi eliminada!!');window.location.href='index.php'</script>";
          } else {
               echo "<script language='javascript' type='text/javascript'>alert('Não existe o utilizador que Indicou!');window.location.href='profile.php'</script>";
          }
          $stmt2->close();
     }

     $stmt->close();
}


function inserirFilme($conn)
{
     $titulo = $conn->real_escape_string($_POST['titulo']);
     $genero = $conn->real_escape_string($_POST['genero']);
     $estado = $conn->real_escape_string($_POST['estado']);
     $sinopse = $_POST['sinopse'];
     if ($titulo === "") {

          echo "<script language='javascript' type='text/javascript'>alert('Defina o titulo do filme');window.location.href='gestor.php'</script>";
     }
     $foto = $_POST['image'];
     if ($foto == null) {
          echo "<script language='javascript' type='text/javascript'>alert('Não selecionou imagem!');window.location.href='gestor.php'</script>";
     }
     if (isset($_POST["submit"])) {
          $check = getimagesize($_FILES["image"]["tmp_name"]);
          if ($check !== false) {
               $image = $_FILES['image']['tmp_name'];
               $imgContent = addslashes(file_get_contents($image));

               $sql = "INSERT INTO filmes VALUES ( '$titulo','$genero','$estado','$sinopse','$imgContent')";
               if ($conn->query($sql) === TRUE) {
                    wh_log("Filme adiciono :" . $titulo);
                    header('location:gestor.php');
               } else {
                    echo "<script language='javascript' type='text/javascript'>alert('Algo correu mal!');window.location.href='gestor.php'</script>";
               }
          }
     }
}
function apagarFilme($conn)
{
     $titulo = $conn->real_escape_string($_POST['titulo']);
     $stmt = $conn->prepare('SELECT r.idFilme, r.idReserva,r.idUser,r.estado FROM Reservas r, Filmes f WHERE r.idFilme=? ORDER BY r.idReserva DESC');
     $stmt->bind_param("s", $titulo);
     $stmt->execute();
     $result = $stmt->get_result();
     if ($result->num_rows > 0) {
          $row = mysqli_fetch_array($result);
          //echo "<script language='javascript' type='text/javascript'>alert('Existe uma reserva desse filme Ativa!');window.location.href='gallery.php'</script>";
          if ($row['estado'] != "Em Reserva") {
               $estado = "Desativado";
               $stmt2 = $conn->prepare("UPDATE Filmes SET estado=? WHERE titulo=?");
               $stmt2->bind_param("ss", $estado, $titulo);
               $stmt2->execute();

               if ($stmt2->affected_rows === 1) {
                    wh_log("Filme Desativado :" . $titulo);
                    header('location:gestor.php');
               } else {
                    echo "Error deleting record: " . $conn->error;
               }
               $stmt2->close();
          } else {
               echo "<script language='javascript' type='text/javascript'>alert('Existe uma reserva desse filme Ativa!');window.location.href='gestor.php'</script>";
          }
     } else {
          $estado = "Desativado";
          $stmt2 = $conn->prepare("UPDATE Filmes SET estado=? WHERE titulo=?");
          $stmt2->bind_param("ss", $estado, $titulo);
          $stmt2->execute();

          if ($stmt2->affected_rows === 1) {
               wh_log("Filme Desativado :" . $titulo);
               header('location:gestor.php');
          } else {
               echo "<script language='javascript' type='text/javascript'>alert('Não existe o filme que Indicou!');window.location.href='gestor.php'</script>";
          }
          $stmt2->close();
     }

     $stmt->close();
}

function editarFilme($conn)
{
     $titulo = $conn->real_escape_string($_POST['titulo']);
     $genero = $conn->real_escape_string($_POST['genero']);
     $estado = $conn->real_escape_string($_POST['estado']);
     $sinopse = $_POST['sinopse'];
     if ($titulo === "") {

          echo "<script language='javascript' type='text/javascript'>alert('Defina o titulo do filme');window.location.href='gestor.php'</script>";
     }
     $foto = $_POST['image'];
     if ($foto == null) {
          echo "<script language='javascript' type='text/javascript'>alert('Não selecionou imagem!');window.location.href='gestor.php'</script>";
     }

     $check = getimagesize($_FILES["image"]["tmp_name"]);
     if ($check !== false) {

          $image = $_FILES['image']['tmp_name'];
          $imgContent = addslashes(file_get_contents($image));


          $sql = "UPDATE Filmes SET genero='$genero',image='$imgContent',sinopse='$sinopse' WHERE titulo = '$titulo' ";

          if ($conn->query($sql) === TRUE) {
               wh_log("Filme Editado :" . $titulo);
               echo header('location:gestor.php');
          } else {
               echo "Error updating record: " . $conn->error;
          }
     }
}
function inserirReserva($conn)
{
     $titulo = $conn->real_escape_string($_POST['titulo']);

     $stmt = $conn->prepare('SELECT * FROM Filmes WHERE titulo = ?');
     $stmt->bind_param('s', $titulo);

     $stmt->execute();
     $result = $stmt->get_result();
     if ($result->num_rows > 0) {
          $row = $result->fetch_assoc();
          if ($row['estado'] === "Disponivel") {
               $conta = $conn->real_escape_string($_POST['conta']);
               $estado = $conn->real_escape_string("Em Reserva");

               $stmt1 = $conn->prepare('INSERT INTO Reservas (idFilme, idUser, estado) VALUES (?,?,?)');
               $stmt1->bind_param('sss', $titulo, $conta, $estado);
               $stmt1->execute();
               if ($stmt1->affected_rows === 1) {

                    $estado = "Reservado";
                    $stmt2 = $conn->prepare('UPDATE  Filmes  SET estado=? WHERE titulo=?');
                    $stmt2->bind_param('ss', $estado, $titulo);
                    $stmt2->execute();

                    if ($stmt2->affected_rows === 1) {
                         wh_log("Filme reservado :" . $titulo . ", por : .$conta");
                         header('location:myreservas.php');
                         $stmt2->close();
                    } else {
                         echo "<script language='javascript' type='text/javascript'>alert('Erro a mudar estado de filme!');window.location.href='gallery.php'</script>";
                         $stmt2->close();
                    }
                    $stmt1->close();
               } else {
                    echo "<script language='javascript' type='text/javascript'>alert('Não é possivel Reservar o filme indicado!');window.location.href='gallery.php'</script>";
                    echo "Erro: <br>" . $conn->error;
                    $stmt1->close();
               }
          } else {
               echo "<script language='javascript' type='text/javascript'>alert('Não é possivel Reservar o filme indicado!');window.location.href='gallery.php'</script>";
          }
     } else {
          echo "<script language='javascript' type='text/javascript'>alert('Não existe esse filme!');window.location.href='gallery.php'</script>";
     }
     $stmt->close();
}
function devolverReserva($conn)
{
     $conta = ($_POST['conta']);
     $titulo = $_POST['titulo'];
     $estado = "Devolvido";
     $stmt = $conn->prepare('UPDATE  RESERVAS SET estado=? WHERE idFilme=? AND idUser=?');
     $stmt->bind_param('sss', $estado, $titulo, $conta);

     $stmt->execute();
     if ($stmt->affected_rows === 1) {
          $estado = "Disponivel";
          $stmt2 = $conn->prepare('UPDATE  Filmes  SET estado=? WHERE titulo=?');
          $stmt2->bind_param('ss', $estado, $titulo);
          $stmt2->execute();

          if ($stmt2->affected_rows === 1) {
               wh_log("Filme devolvido  :" . $titulo . ", por : " . $conta);
               header('location:myreservas.php');
               $stmt2->close();
          } else {
               echo "<script language='javascript' type='text/javascript'>alert('Erro a mudar estado de filme!');window.location.href='myreservas.php'</script>";
               $stmt2->close();
          }
          $stmt->close();
     } else {
          echo "<script language='javascript' type='text/javascript'>alert('Não existe filme!');window.location.href='myreservas.php'</script>";
     }
     $stmt->close();
}
function editarUser($conn)
{
     session_start();
     $conta = $_SESSION['email'];
     $nome = $conn->real_escape_string($_POST['nome']);
     $password = $conn->real_escape_string($_POST['password']);
     $morada = $conn->real_escape_string($_POST['morada']);
     $idade =  ($_POST['idade']);

     $uppercase = preg_match('@[A-Z]@', $password);
     $lowercase = preg_match('@[a-z]@', $password);
     $number    = preg_match('@[0-9]@', $password);
     $number2    = preg_match('@[0-9]@', $idade);
     $specialChars = preg_match('@[^\w]@', $password);
     if (!preg_match("/^[a-zA-Z ]*$/", $nome)) {
          echo "<script language='javascript' type='text/javascript'>alert('Só aceita letras no nome!');window.location.href='index.php'</script>";
     } elseif (!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
          echo 'Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.';
          echo "<script language='javascript' type='text/javascript'>alert('PassWord INvalida');window.location.href='index.php'</script>";
     } elseif (!$number2 ||  $idade < 0) {
          echo "<script language='javascript' type='text/javascript'>alert('A idade deve ser um Numero e/ou Positivo!');window.location.href='index.php'</script>";
     } else {
          $pw_hash = password_hash($password, PASSWORD_DEFAULT);
          $stmt = $conn->prepare('UPDATE Utilizadores SET nome=?, password=?, morada=?, idade=? WHERE email=?');
          $stmt->bind_param('sssis', $nome, $pw_hash, $morada, $idade, $conta);

          $stmt->execute();


          if ($stmt->affected_rows === 1) {
               wh_log("Utilizaor Editado :" . $conta);
               header('location:profile.php');
          } else {
               echo "<script language='javascript' type='text/javascript'>alert('Algo correu mal!');window.location.href='profile.php'</script>";
          }
     }
     $stmt->close();
}


function recuperar($conn)
{


     $email = $conn->real_escape_string($_POST['email']);
     require './vendor/autoload.php';
     //Create a new PHPMailer instance
     $mail = new PHPMailer;
     //Charset for the message
     $mail->CharSet = 'utf-8';
     //Tell PHPMailer to use SMTP
     $mail->isSMTP();
     //Enable SMTP debugging
     $mail->SMTPDebug = 2; //0 = off (for production use); 1 = client messages; 2 = client and server messages
     //Set the hostname of the mail server
     $mail->Host = 'smtp.gmail.com';
     //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
     $mail->Port = 587;
     //Set the encryption system to use - ssl (deprecated) or tls
     $mail->SMTPSecure = 'tls';
     //Whether to use SMTP authentication
     $mail->SMTPAuth = true;
     //Username to use for SMTP authentication - use full email address for gmail
     $mail->Username = "sawestgipp@gmail.com";
     //Password to use for SMTP authentication
     $mail->Password = "SAW2020ipp";
     //Set who the message is to be sent from
     $mail->setFrom('seg.apli.web@gmail.com', 'Segurança das Aplicações Web');
     //Set who the message is to be sent to
     $mail->addAddress($email, '   ');
     //Set the subject line
     $mail->Subject = 'SAW.com';
     //allow html code in body message
     $mail->IsHTML(true);
     //Body text for the message
     $mail->Body = "

     Siga o seguinte link para recuperar a sua conta:
     http://saw.com/recuperar.php?email=$email";
     //aditional options to SMTP
     $mail->SMTPOptions = array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true));

     if (!$mail->send()) {
          echo "Mailer Error: " . $mail->ErrorInfo;
     } else {
          echo "Message sent!";
     }
     wh_log("Conta repuperada :" . $email);
     header('location:index.php');
}
function editarprivilegios($conn)
{
     $email = $conn->real_escape_string($_POST['email']);
     $tipo = $conn->real_escape_string($_POST['tipoUser']);
     $stmt = $conn->prepare('UPDATE Utilizadores SET tipoUser=? WHERE email=?');
     $stmt->bind_param('ss', $tipo, $email);

     $stmt->execute();


     if ($stmt->affected_rows === 1) {
          wh_log("Privilegios alterados ao user :" . $email);
          echo "<script language='javascript' type='text/javascript'>alert('Previlégios alterados!');window.location.href='gestorUsers.php'</script>";
     } else {
          echo "<script language='javascript' type='text/javascript'>alert('Erro ao alterar Previlégios!');window.location.href='gestorUsers.php'</script>";
     }
}

function eliminarReserva($conn)
{

     $titulo = $_POST['titulo'];
     $estadoAtual = "Em Reserva";
     $estado = "Devolvido";
     $stmt = $conn->prepare('UPDATE  RESERVAS SET estado=? WHERE idFilme=? AND estado=?');
     $stmt->bind_param('sss', $estado, $titulo, $estadoAtual);

     $stmt->execute();
     if ($stmt->affected_rows === 1) {
          $estado = "Disponivel";
          $stmt2 = $conn->prepare('UPDATE  Filmes  SET estado=? WHERE titulo=?');
          $stmt2->bind_param('ss', $estado, $titulo);
          $stmt2->execute();

          if ($stmt2->affected_rows > 0) {
               wh_log("Filme devolvido :" . $titulo);
               echo "<script language='javascript' type='text/javascript'>alert('Reserva Eliminada!');window.location.href='gestorReservas.php'</script>";
               $stmt2->close();
          } else {
               echo "<script language='javascript' type='text/javascript'>alert('Erro a mudar estado de filme!');window.location.href='gestorReservas.php'</script>";
               $stmt2->close();
          }
          $stmt->close();
     } else {
          echo "<script language='javascript' type='text/javascript'>alert('Não existe filme!');window.location.href='gestorReservas.php'</script>";
     }
     $stmt->close();
}
function banir($conn)
{

     $email = $_POST['email'];

     $stmt = $conn->prepare('SELECT r.idFilme, r.idReserva,r.idUser,r.estado FROM Reservas r, Filmes f WHERE r.idUser=? ORDER BY r.estado DESC');
     $stmt->bind_param("s", $email);
     $stmt->execute();
     $result = $stmt->get_result();
     if ($result->num_rows > 0) {
          $row = mysqli_fetch_array($result);
          //echo "<script language='javascript' type='text/javascript'>alert('Existe uma reserva desse filme Ativa!');window.location.href='gallery.php'</script>";
          if ($row['estado'] === "Devolvido") {
               $estado = "Desativado";
               $stmt2 = $conn->prepare("UPDATE Utilizadores SET tipoUser=? WHERE email=?");
               $stmt2->bind_param("ss", $estado, $email);
               $stmt2->execute();

               if ($stmt2->affected_rows === 1) {

                    wh_log("Conta eliminada :" . $email);
                    echo "<script language='javascript' type='text/javascript'>alert('A  conta foi eliminada!!');window.location.href='gestorUsers.php'</script>";
               } else {
                    echo "<script language='javascript' type='text/javascript'>alert('Algo correu mal!');window.location.href='gestorUsers.php'</script>";
               }
               $stmt2->close();
          } else {
               echo "<script language='javascript' type='text/javascript'>alert('Existem Filmes reservados pelo cliente indicado!');window.location.href='gestorUsers.php'</script>";
          }
     } else {
          $estado = "Desativado";
          $stmt2 = $conn->prepare('UPDATE Utilizadores SET tipoUser=? WHERE email=?');
          $stmt2->bind_param("ss", $estado, $email);
          $stmt2->execute();

          if ($stmt2->affected_rows === 1) {
               wh_log("Conta eliminada :" . $email);
               echo "<script language='javascript' type='text/javascript'>alert('A  conta foi eliminada!!');window.location.href='gestorUsers.php'</script>";
          } else {
               echo "<script language='javascript' type='text/javascript'>alert('Não existe o utilizador que Indicou!');window.location.href='gestorUsers.php'</script>";
          }
          $stmt2->close();
     }

     $stmt->close();
}

function updateFoto($conn)
{
     session_start();
     $email = $_SESSION['email'];
     $foto = $_POST['image'];
     if ($foto == null) {
          echo "<script language='javascript' type='text/javascript'>alert('Não selecionou imagem!');window.location.href='Profile.php'</script>";
     }
     $check = getimagesize($_FILES["image"]["tmp_name"]);


     if ($check !== false) {

          $image = $_FILES['image']['tmp_name'];
          $imgContent = addslashes(file_get_contents($image));

          $sql = "UPDATE Utilizadores SET image='$imgContent' WHERE email='$email'";

          if ($conn->query($sql) === TRUE) {
               wh_log("Alteracao de foto de perfil por : " . $email);
               echo header('location:Profile.php');
          } else {
               echo "<script language='javascript' type='text/javascript'>alert('Não selecionou imagem!');window.location.href='Profile.php'</script>";
          }
     }
}


$conn->close();

?>