<!DOCTYPE HTML>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<?php
include 'conection.php';
session_start();
include 'modals.php'; ?>
<?php
$conta = $_SESSION['email'];
if ($conta == null) {
   header('location:index.php');
}
?>

<html>


<head>
   <title>SAW </title>
   <meta charset="utf-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1" />
   <link rel="stylesheet" href="assets/css/main.css" />
</head>

<body>


   <div class="page-wrap">

      <!-- Nav -->
      <?php include 'nav.php'; ?>

      <!-- Main -->

      <section id="main">

         <h3 class="display-4" style="text-align: center;">Perfil de Utilizador</h3>
         <hr>

         <section>

            <div class="container" style="min-height: 720px">
               <?php
               $conta = $_SESSION['email'];
               if ($conta == null) {

                  header('location:index.php');
               }


               $stmt = $conn->prepare('SELECT * FROM utilizadores WHERE email=?');
               $stmt->bind_param('s', $conta);

               $stmt->execute();
               $result = $stmt->get_result();
               if ($result->num_rows === 1) {


                  $row = mysqli_fetch_array($result);
               }
               ?>
               <div class="row">
                  <div class="col-lg-4 order-lg-1 text-center">
                     <img style="max-height: 250px;" <?php echo ' src="data:image/jpeg;base64,' . base64_encode($row['image']) . '"'; ?> />
                     <form action="/sql.php" class="form" role="form" enctype="multipart/form-data" autocomplete="off" id="formLogin" novalidate="" method="POST">
                        <input type="hidden" name="metodo" value="updateFoto">
                        <label class="custom-file">
                           <input type="file" id="image" name="image" class="custom-file-input">
                           <span class="custom-file-control">Escolher nova foto</span>
                        </label>
                        <button type="submit" style="max-height: 35px; margin-top:20px" value="UPLOAD" class="btn btn-success" id="btnDevolver">OK</button>
                     </form>

                  </div>
                  <div class="col-lg-8 order-lg-2">
                     <ul class="nav nav-tabs">
                        <li class="nav-item">
                           <h2>Perfil</h2>
                        </li>

                     </ul>
                     <div class="tab-content py-4">

                        <div class="tab-pane active" id="profile">
                           <div class="row">
                              <div>
                                 <h3 class="mb-3">Nome : <?php echo $row['nome']; ?></h3><br>



                                 <h6 class="mb-3">Email : <?php echo $row['email'] ?></h6><br>


                                 <h6 class="mb-3">Morada : <?php echo $row['morada'] ?></h6><br>



                                 <h6 class="mb-3">Idade : <?php echo $row['idade'] ?></h6>

                              </div>
                           </div>
                           <br><br>
                           <br><br>
                           <br>
                           <br>
                           <div class="container">
                              <div class="row">
                                 <div class="col">

                                 </div>
                                 <div class="col-6">
                                    <button type="button" data-toggle="modal" data-target="#EditarUser" class="btn btn-secondary">Editar Conta</button></a>
                                 </div>
                                 <div class="col">

                                    <button type="button" data-toggle="modal" data-target="#eliminarUser" class="btn btn-danger">Emilinar Conta</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>

         </section>
         <?php include 'footer.php'; ?>
      </section>
   </div>



</body>


</html>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>