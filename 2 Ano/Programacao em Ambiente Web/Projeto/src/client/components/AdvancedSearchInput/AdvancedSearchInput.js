import React, { useState } from 'react'

import { stringify } from 'query-string'

import styles from './AdvancedSearchInput.module.sass'
import Dropdown from './Dropdown'
import Range from '../inputs/Range/Range'

const options = [
	{
		name: 'Lacoste'
	},
	{
		id: 'Fred Perry',
		name: 'Fred Perry'
	},
	{
		id: 'Calvin Klein',
		name: 'Calvin Klein'
	},
	{
		id: 'Levis',
		name: 'Levis'
	},
	{
		id: 'Iceplay',
		name: 'Iceplay'
	}
]

const AdvancedSearchInput = () => {
	const [searchData, setSearchData] = useState({ marca:'', modelo: '', tags: ['highlights', 'homepage'] })
	const [roomsNumber, setRoomsNumber] = useState(null)

	const [advancedSearch, setAdvancedSearch] = useState(false)
	const handleSubmit = (e) => {
		e.preventDefault()
		const searchString = stringify(searchData, {arrayFormat: 'comma'})
		console.log(searchString)
	}
	return (
		<div className={ styles['root'] }>
			{ advancedSearch && (
				<form onSubmit={ handleSubmit } className={ styles['advanced-form'] }>
					<div>
						<h5>Advanced Search</h5>
						<div>
							<h4>Tamanho que procura</h4>
							<Range
								value={ searchData.marca }
								onChange={ (e) => {
									setSearchData({
										...searchData,
										marca: e.target.value
									})
								} }
							/>
						</div>
						<div>
							<h4>Modelo</h4>
							<input
								type="text"
								placeholder="Modelo"
								value={ searchData.modelo }
								onChange={ (e) => {
									setSearchData({
										...searchData,
										modelo: e.target.value
									})
								} } />
						</div>
						<div className={ styles['actions'] }>
							<button type="submit" className={ styles['search-button'] }><b>Find</b></button>
						</div>
					</div>
				</form>
			) }
			<form onSubmit={ handleSubmit } className={ styles['form'] }>
				<div className={ styles['dropdown-container'] }>
					<Dropdown
						className={ styles['dropdown'] }
						value={ roomsNumber || 'Brands' }
						options={ options }
						onChange={ (value) => {
							setRoomsNumber(value)
						} }
					/>
				</div>
				<div className={ styles['input-container'] }>
					<input placeholder='Type the type of piece you want' />
				</div>
				<button type="submit" className={ styles['find-button'] }><b>Find</b></button>
				<button
					type="button"
					className={ styles['more-options'] }
					onClick={ () => {
						setAdvancedSearch(!advancedSearch)
					} }
				>Options</button>
			</form>
		</div>
	)
}

export default AdvancedSearchInput