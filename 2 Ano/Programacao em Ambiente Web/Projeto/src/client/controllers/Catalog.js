import React, { useState } from 'react'
import Grid from '../components/Grid/Grid'

import styles from './layout.module.sass'
import useApi from '../hooks/useApi'
import { useLocation } from 'react-router-dom'
import { parse } from 'query-string'
import Range from '../components/inputs/Range/Range'
import DetailsCard from '../components/cards/DetailsCard/DetailsCard'
import { Link } from 'react-router-dom'

const Homepage = () => {
	const [searchData, setSearchData] = useState({ marca: '', modelo: '', tags: ['highlights', 'homepage'] })
	const { search } = useLocation()

	const parseParams = parse(search, { arrayFormat: 'comma' })
	const filters = {
		...parseParams,
		page_size: undefined,
		page: undefined
	}

	const [data, loading, error, reload] = useApi(`/api/articles`, parseParams.page_size || 24, parseParams.page || 0, filters, true)
	const items = !error ? (data || { list: [] }).list || [] : []

	const handleSubmit = (e) => {
		e.preventDefault()
	}

	return (
		<>
			<header className={styles['header']}>
				<div>
					Catálogo de Roupa
				</div>
			</header>
			<div className={`${styles['max-width']} ${styles['aside-col']}`}>
				<div>
					<Grid cols={[1, 1, 1]}>
						{items.map((item, index) => (
							<Link to={`articles/${item.id}` } className={styles['title']}>
								<DetailsCard key={index} className={styles['repeat']} item={item} />
							</Link>
						))}
					</Grid>
				</div>
				<div>
					<div className={styles['aside-header']}><small>Filters</small></div>
					<form onSubmit={handleSubmit}>
						<div>
							<h4 className={styles['aside-title']}>Tamanho que procura</h4>
							<Range
								value={searchData.marca}
								min="0"
								max="6"
								onChange={(e) => {
									setSearchData({
										...searchData,
										marca: e.target.value
									})
								}}
							/>
						</div>
						<div>
							<h4 className={styles['aside-title']}>Modelo</h4>
							<input
								type="text"
								placeholder="Tipo de modelo que procura"
								value={searchData.location}
								onChange={(e) => {
									setSearchData({
										...searchData,
										modelo: e.target.value
									})
								}} />
						</div>
						<div className={styles['actions']}>
							<button type="submit" className={styles['search-button']}><b>Find</b></button>
						</div>
					</form>
				</div>
			</div>
		</>
	)
}

export default Homepage