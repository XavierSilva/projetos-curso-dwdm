import React from 'react'
import styles from './layout.module.sass'
import { parse } from 'query-string'
import useApi from '../hooks/useApi'
import { useHistory, Link, useLocation, useParams } from 'react-router-dom'
import DetailsCard2 from '../components/cards/DetailsCard2/DetailsCard'
import { PromiseProvider } from 'mongoose'
import Homepage from './Homepage'
import Header from '../components/Header/Header'
import Grid from '../components/Grid/Grid'
import ThumbCard from '../components/cards/ThumbCard/ThumbCard'


const Details = (props) => {

	const { articleId } = useParams()
	//console.log(articleId)
	const [data, loading, error] = useApi(`/api/articles/${ articleId }`)


	return (
		<>
			<header className={styles['header']}>
				<div>
					Detalhes de Roupa
				</div>
			</header>

			<main>
				{loading && <div>Loading...</div>}
				{error && <div>{error.message || 'Error'}</div>}
				{data && (
						<DetailsCard2 key={0} item={data} />

				)}
			</main>

		</>
	)
}

export default Details