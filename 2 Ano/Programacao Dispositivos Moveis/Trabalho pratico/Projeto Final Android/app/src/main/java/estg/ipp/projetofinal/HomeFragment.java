package estg.ipp.projetofinal;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.lang.*;
import java.net.MalformedURLException;
import java.net.URL;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private Context context;
    ImageView imageView = null;
    TextView txtTemp;
    TextView txtCidade;
    private static final int REQUEST_FINE_LOCATION = 100;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private Geocoder mGeoCoder;
    Bitmap bitmaps = null;

    String BaseUrl = "http://api.openweathermap.org/";
    String AppId = "afebaef556b6934b38a9af1f33c16001";
    public String lat;
    public String lon;
    String units ="metric";
    Button btnComecar ;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        this.context = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFusedLocationClient = getFusedLocationProviderClient(getActivity());
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        txtTemp = v.findViewById(R.id.txtTemp);
        txtCidade = v.findViewById(R.id.txtCidade);
        btnComecar = v.findViewById(R.id.btnComecar);
        btnComecar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = ((FragmentActivity) getContext()).getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                AddFragment addFragment = new AddFragment();
                transaction.replace(R.id.contentor, addFragment);
                transaction.commit();
            }
        });
        imageView = v.findViewById(R.id.imageView);
        getLastLocation();

        return v;

    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    lat = String.valueOf(location.getLatitude());
                    lon = String.valueOf(location.getLongitude());
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(BaseUrl)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    com.androidmads.openweatherapi.WeatherService service = retrofit.create(com.androidmads.openweatherapi.WeatherService.class);
                    Call<WeatherResponse> call = service.getCurrentWeatherData(lat, lon, AppId,units);
                    call.enqueue(new Callback<WeatherResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<WeatherResponse> call, @NonNull Response<WeatherResponse> response) {
                            if (response.code() == 200) {
                                WeatherResponse weatherResponse = response.body();
                                assert weatherResponse != null;

                        int tempo = (int) weatherResponse.main.temp;
                                String cidade = weatherResponse.name;
                                String ico = weatherResponse.weather.get(0).icon;
                                txtTemp.setText(tempo + "º");
                                txtCidade.setText(cidade);

                                //AsyncTask asyncTask = new AsyncTask();
                                AsyncTaskExample asyncTask = new AsyncTaskExample();
                                String stringImg = "http://openweathermap.org/img/w/" + ico +".png";
                                URL url = null;
                                try {
                                    url = new URL(stringImg);
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }
                                asyncTask.execute(url);

                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<WeatherResponse> call, @NonNull Throwable t) {
                            txtTemp.setText(t.getMessage());
                        }
                    });

                }
            }
        })
                .addOnFailureListener(getActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG).show();
                    }
                });

    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_FINE_LOCATION);
    }
    class AsyncTaskExample extends AsyncTask<URL, Integer, Bitmap> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Bitmap doInBackground(URL... urls) {
            for (int i = 0; i < urls.length; i++) {
                try {
                    bitmaps = BitmapFactory.decodeStream(urls[i].openConnection().getInputStream());


                } catch (IOException e) {
                }
            }

            return bitmaps;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);


            imageView.setImageBitmap(bitmap);

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

        }


    }

}


