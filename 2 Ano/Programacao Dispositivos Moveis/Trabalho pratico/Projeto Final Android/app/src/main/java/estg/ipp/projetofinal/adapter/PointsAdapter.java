package estg.ipp.projetofinal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import estg.ipp.projetofinal.R;
import estg.ipp.projetofinal.database.entity.Points;


public class PointsAdapter extends RecyclerView.Adapter<PointsAdapter.PointsHolder> {
    private List<Points> points;

    public PointsAdapter(Context context) {

    }

    public void setPoints(List<Points> points){
        this.points = points;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PointsAdapter.PointsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.layout_list_atividades, parent, false);
        return new PointsHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PointsAdapter.PointsHolder holder, int position) {
        if(points != null){
            Points point = points.get(position);
            if(holder != null){
                int data = point.getId();
                int inicio = point.getIdAtividade();
                double fim = point.getLatitude();
                holder.txtData.setText("id: " + data);
                holder.txtInicio.setText("idAtividade: " + inicio);
                holder.txtFim.setText("Latitude: " + fim);
            }

        }
    }


    @Override
    public int getItemCount() {
        if (points != null)
            return points   .size();
        return 0;
    }

    public class PointsHolder extends RecyclerView.ViewHolder {
        TextView txtData;
        TextView txtInicio;
        TextView txtFim;

        public PointsHolder(@NonNull View itemView) {
            super(itemView);
            txtData = itemView.findViewById(R.id.txtData);
            txtInicio = itemView.findViewById(R.id.txtInicio);
            txtFim = itemView.findViewById(R.id.txtFim);
        }
    }
}
