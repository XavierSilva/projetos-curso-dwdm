package estg.ipp.projetofinal.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import estg.ipp.projetofinal.database.entity.Points;
import estg.ipp.projetofinal.repository.PointsRepository;

public class PointsViewModel extends AndroidViewModel {
    private PointsRepository repositorio;
    private LiveData<List<Points>> allPoints;
    private LiveData<List<Points>>   pointsByID;
    private int id;


    public PointsViewModel(@NonNull Application application) {
        super(application);
        repositorio = new PointsRepository(application);
        allPoints = repositorio.getAllPoints();
        //pointsByID =  repositorio.getPointsByID(id);
    }

    public LiveData<List<Points>> getAllPoints() {
        return allPoints;
    }
    public LiveData<List<Points>> getPointsByID(int idAtividade) {
        return pointsByID = repositorio.getPointsByID(idAtividade);
    }

    public void insere(Points point){
        repositorio.insertPoints(point);
    }
}
