package estg.ipp.projetofinal.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import estg.ipp.projetofinal.database.dao.AtividadeDao;
import estg.ipp.projetofinal.database.dao.PointsDao;
import estg.ipp.projetofinal.database.entity.Atividade;
import estg.ipp.projetofinal.database.entity.Points;

@Database(entities = { Atividade.class,Points.class,}, version = 2, exportSchema = false)
public abstract class AtividadeDataBase extends RoomDatabase {

    private static AtividadeDataBase INSTANCE;

    //declarar aqui
    public abstract PointsDao atividadePointsDao();
    public abstract AtividadeDao atividadeDao();

    static final Migration MIGRATON_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {

        }
    };

    public static AtividadeDataBase getInstance(final Context context) {
        if (INSTANCE == null) {
            synchronized (AtividadeDataBase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AtividadeDataBase.class, "atividades.db")
                            .addMigrations(MIGRATON_1_2)
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
