package estg.ipp.projetofinal.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import estg.ipp.projetofinal.database.AtividadeDataBase;
import estg.ipp.projetofinal.database.dao.PointsDao;
import estg.ipp.projetofinal.database.entity.Points;

public class PointsRepository {
   private PointsDao pointsDao;
    private LiveData<List<Points>> allPoints;
    private LiveData<List<Points>>  pointsByID;

    public PointsRepository(Application application) {
        AtividadeDataBase db = AtividadeDataBase.getInstance(application);
        pointsDao = db.atividadePointsDao();
        allPoints = pointsDao.getAllPoints();

    }

    public void insertPoints( Points p){
        new InsertAsync(pointsDao).execute(p);
    }

    public LiveData<List<Points>> getAllPoints(){
        return allPoints;
    }
    public LiveData<List<Points>>  getPointsByID(int idAtividade){
        return pointsByID =  pointsDao.getPointsByID(idAtividade);
    }
    private static class InsertAsync extends AsyncTask<Points,Void,Void> {
        private PointsDao pointsDao;

        public InsertAsync(PointsDao pointsDao) {
            this.pointsDao = pointsDao;
        }

        @Override
        protected Void doInBackground(Points... atividadesPoints) {
            pointsDao.inserePoints(atividadesPoints[0]);
            return null;
        }
    }
}
