package estg.ipp.projetofinal;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import estg.ipp.projetofinal.viewModel.AtividadeViewModel;

public class MainActivity extends AppCompatActivity implements Cliques {
    private AtividadeViewModel model;
    private Button btnLogin;
    private Button btn_registo;
    private EditText edt_email;
    private EditText edt_password;
    private Button btn_resetPassword;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        model = ViewModelProviders.of(this).get(AtividadeViewModel.class);
        btnLogin = findViewById(R.id.btnLogin);
        btn_registo = findViewById(R.id.btnRegisto);
        edt_email = findViewById(R.id.txtEmail);
        edt_password = findViewById(R.id.txtPw);
        //btn_resetPassword = findViewById(R.id.btnresetPassword);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*String email = edt_email.getText().toString().trim();
                String password = edt_password.getText().toString().trim();*/
                String email ="admin@admin.com";
                String password = "adminadmin";
                if(email.length() == 0 ||password.length() == 0 ){
                    alert("Campos de email/password vazios!");
                }else {
                    login(email, password);
                }

            }
        });
        Button btnRegisto = findViewById(R.id.btnRegisto);
        btnRegisto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,  RegistoActivity.class);
                startActivity(i);
            }
        });



    }
    private void login(String email, String password) {
        Toast.makeText(MainActivity.this, email, Toast.LENGTH_SHORT).show();
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            alert("user correto");
                            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                            startActivity(intent);
                        } else {
                            alert("email ou pass errado");
                        }
                    }
                });
    }

    private void alert(String s) {
        Toast.makeText(MainActivity.this, s,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        auth = Conection.getFirebaseAuth();
    }
}
