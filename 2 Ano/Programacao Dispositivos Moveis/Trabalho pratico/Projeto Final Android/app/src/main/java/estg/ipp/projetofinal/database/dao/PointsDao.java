package estg.ipp.projetofinal.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import estg.ipp.projetofinal.database.entity.Points;

@Dao
public interface PointsDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void inserePoints(Points... atividadePoints);

    @Query("SELECT * FROM Points")
    LiveData<List<Points>> getAllPoints();

    @Query("Select * from Points  WHERE idAtividade = :idAtividade")
    LiveData<List<Points>> getPointsByID(int idAtividade);


}
