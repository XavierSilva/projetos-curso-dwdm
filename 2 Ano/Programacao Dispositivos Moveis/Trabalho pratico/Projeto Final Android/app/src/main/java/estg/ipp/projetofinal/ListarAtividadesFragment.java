package estg.ipp.projetofinal;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import estg.ipp.projetofinal.adapter.AtividadeAdapter;
import estg.ipp.projetofinal.database.entity.Atividade;
import estg.ipp.projetofinal.viewModel.AtividadeViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListarAtividadesFragment extends Fragment {

    private AtividadeViewModel model;
    private AtividadeAdapter adapter;
    private final int ADICIONAR_REQUEST_CODE = 1;
    RecyclerView recyclerView ;


    public ListarAtividadesFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_listar_atividades, container, false);
        recyclerView = v.findViewById(R.id.mRecycler);
        adapter = new AtividadeAdapter(getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        model = ViewModelProviders.of(this).get(AtividadeViewModel.class);
        model.getAllAtividades().observe(this, new Observer<List<Atividade>>() {
            @Override
            public void onChanged(List<Atividade> atividades) {
                adapter.setAtividades(atividades);
            }
        });
        return v;
    }

}
