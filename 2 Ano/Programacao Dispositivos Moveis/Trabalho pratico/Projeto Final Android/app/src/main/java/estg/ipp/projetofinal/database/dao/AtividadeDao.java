package estg.ipp.projetofinal.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import estg.ipp.projetofinal.database.entity.Atividade;

@Dao
public interface AtividadeDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insereAtividade(Atividade... atividades);

    @Query("SELECT * FROM atividade ORDER BY idAtividade DESC")
    LiveData<List<Atividade>> getAllAtividades();

    @Query("SELECT * FROM atividade WHERE idAtividade =:idAtividade")
    LiveData<List<Atividade>> getAtividate(int idAtividade);

    @Delete
    void deleteAtividade(Atividade... atividades);

    @Update
    void updateAtividade(Atividade... atividades);

}
