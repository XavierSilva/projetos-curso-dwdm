package estg.ipp.projetofinal.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import estg.ipp.projetofinal.database.AtividadeDataBase;
import estg.ipp.projetofinal.database.dao.AtividadeDao;
import estg.ipp.projetofinal.database.entity.Atividade;

public class AtividadeRepository {
    private AtividadeDao atividadeDao;
    private LiveData<List<Atividade>> allAtividades;
    private LiveData<List<Atividade>> getAtividade;

    public AtividadeRepository(Application application) {
        AtividadeDataBase db = AtividadeDataBase.getInstance(application);
        atividadeDao = db.atividadeDao();
        allAtividades = atividadeDao.getAllAtividades();
    }

    public void insertAtividade( Atividade a){
        new InsertAsync(atividadeDao).execute(a);
    }
    public  void deleleAtividade(Atividade a){
        new DeleteAsync(atividadeDao).execute(a);
    }
    public  void updateAtividade(Atividade a){
       new UpdateAsync(atividadeDao).execute(a);


    }

    public LiveData<List<Atividade>> getAllAtividades(){
        return allAtividades;
    }


    public LiveData<List<Atividade>> getAtividade(int idAtividade){
        return getAtividade = atividadeDao.getAtividate(idAtividade);
    }

    private static class InsertAsync extends AsyncTask<Atividade,Void,Void> {
        private AtividadeDao atividadeDao;

        public InsertAsync(AtividadeDao atividadeDao) {
            this.atividadeDao = atividadeDao;
        }

        @Override
        protected Void doInBackground(Atividade... atividades) {
            atividadeDao.insereAtividade(atividades[0]);
            return null;
        }
    }
    private static class DeleteAsync extends AsyncTask<Atividade,Void,Void> {
        private AtividadeDao atividadeDao;

        public DeleteAsync(AtividadeDao atividadeDao) {
            this.atividadeDao = atividadeDao;
        }

        @Override
        protected Void doInBackground(Atividade... atividades) {
            atividadeDao.deleteAtividade(atividades[0]);
            return null;
        }
    }

    private static class UpdateAsync extends AsyncTask<Atividade,Void,Void> {

        private AtividadeDao atividadeDao;

        public UpdateAsync(AtividadeDao atividadeDao) {
            this.atividadeDao = atividadeDao;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected Void doInBackground(Atividade... atividades) {

                        atividadeDao.updateAtividade(atividades[0]);


            return null;
        }
    }
}