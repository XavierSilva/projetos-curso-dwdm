package estg.ipp.projetofinal.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import estg.ipp.projetofinal.database.entity.Atividade;
import estg.ipp.projetofinal.repository.AtividadeRepository;

public class AtividadeViewModel extends AndroidViewModel {
    private AtividadeRepository repositorio;
    private LiveData<List<Atividade>> allAtividades;
    private LiveData<List<Atividade>> getAtividade;


    public AtividadeViewModel(@NonNull Application application) {
        super(application);
        repositorio = new AtividadeRepository(application);
        allAtividades = repositorio.getAllAtividades();
    }

    public LiveData<List<Atividade>> getAllAtividades() {
        return allAtividades;
    }

    public void insere(Atividade atividade){
        repositorio.insertAtividade(atividade);
    }
    public void deleteAtividade(Atividade a){
        repositorio.deleleAtividade(a);
    }
    public void updateAtividade(Atividade a)
    {
        repositorio.updateAtividade(a);
    }
        public LiveData<List<Atividade>> getAtividade(int idAtividade) {
            return getAtividade = repositorio.getAtividade(idAtividade);
        }

}