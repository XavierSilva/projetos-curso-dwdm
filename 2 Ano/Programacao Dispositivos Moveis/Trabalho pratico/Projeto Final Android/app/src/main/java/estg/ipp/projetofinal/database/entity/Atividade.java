package estg.ipp.projetofinal.database.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.*;

@Entity (tableName="atividade")
public  class Atividade {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "idAtividade")
    private int id;
    @NonNull
    private String data;
    @NonNull
    private String inicio;

    private String fim;
    private int duracao;
    private int passos;
    private double distancia;

    public Atividade(int id, @NonNull String data, @NonNull String inicio) {
        this.id = id;
        this.data = data;
        this.inicio = inicio;
        this.fim = null;
        this.duracao = 0;
        this.passos = 0;
        this.distancia = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getData() {
        return data;
    }

    public void setData(@NonNull String data) {
        this.data = data;
    }

    @NonNull
    public String getInicio() {
        return inicio;
    }

    public void setInicio(@NonNull String inicio) {
        this.inicio = inicio;
    }


    public String getFim() {
        return fim;
    }

    public void setFim( String fim) {
        this.fim = fim;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public int getPassos() {
        return passos;
    }

    public void setPassos(int passos) {
        this.passos = passos;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }
}
