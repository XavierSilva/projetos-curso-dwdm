package estg.ipp.projetofinal;


import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import estg.ipp.projetofinal.database.entity.Atividade;
import estg.ipp.projetofinal.database.entity.Points;
import estg.ipp.projetofinal.viewModel.AtividadeViewModel;
import estg.ipp.projetofinal.viewModel.PointsViewModel;

import static android.content.Context.SENSOR_SERVICE;
import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddFragment extends Fragment  implements OnMapReadyCallback, SensorEventListener {
    private GoogleMap mGoogleMap;
    private static final int REQUEST_FINE_LOCATION = 100;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;

    private FusedLocationProviderClient mFusedLocationClient2;
    private LocationRequest mLocationRequest2;
    private LocationCallback mLocationCallback2;
    private Geocoder mGeoCoder;
    Chronometer chronometer;
    private  TextView txtLatitude;
    TextView txtLong;
    TextView txtAcc;
    TextView txtGeoCoder;
    TextView txtVelocidade;
    Cliques cliques;
    Context context;
    TextView txtNumPassos;
    TextView txtDistancia;
    public BottomNavigationView navigationView;
    Double distanceInMeters = 0.0;
    private static final String CHANNEL_ID = "01";
    private static final int notificationId = 01;
    private final String ACTION = "pt.ipp.ACTIONUPDATE";
    private static NotificationCompat.Builder mBuilder;

    private NotificationManager notificationManager;

    private SensorManager mSensorManager;
    private Sensor mDetect;
    private int count;
    Boolean ativo = false;
    ArrayList<Double> arrayLatitude = null;
    ArrayList<Double> arrayLongitude = null;

    LiveData<List<Atividade>> Atividades;
    LiveData<List<Atividade>> AtividadesF;

    Button btnStart ;
    Button btnParar ;

    Boolean cont = false;
    private AtividadeViewModel model;
    private AtividadeViewModel modelf;
    private PointsViewModel pointsViewModel;
    Atividade ultimaAtividade;



    public AddFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        try {
            cliques = (Cliques) context;
        } catch (Exception e) {
            Log.e("onAttach", e.toString());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        navigationView=((SecondActivity)this.getActivity()).getNavigationView();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        mFusedLocationClient = getFusedLocationProviderClient(getActivity());
        mFusedLocationClient2 = getFusedLocationProviderClient(getActivity());


        View v = inflater.inflate(R.layout.fragment_add, container, false);
        createNotificationChannel();
        chronometer= v.findViewById(R.id.simpleChronometer); // initiate a chronometer
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.stop();
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.setBase(SystemClock.elapsedRealtime());
        txtGeoCoder = v.findViewById(R.id.txtGeoCoder);
        btnStart = v.findViewById(R.id.btnStart);
        btnParar = v.findViewById(R.id.btnParar);
        navigationView = v.findViewById(R.id.navigationView);
        btnParar.setVisibility(View.INVISIBLE);
        txtDistancia = v.findViewById(R.id.txtDistanciaD);
        this.arrayLatitude = new ArrayList<>();
        this.arrayLongitude = new ArrayList<>();
        mSensorManager = (SensorManager)context.getSystemService(SENSOR_SERVICE);
        mDetect = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        txtNumPassos = v.findViewById(R.id.txtNumPassosD);
        mSensorManager.registerListener(this, mDetect ,SensorManager.SENSOR_DELAY_FASTEST);
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(100); mLocationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {

                for(final Location location : locationResult.getLocations()){
                    //1getGeoCoder(location);
                    addCircle(location.getLatitude(),location.getLongitude());
                }
            }
        };


        mLocationRequest2 = new LocationRequest();
        mLocationRequest2.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest2.setInterval(5000);
        mLocationRequest2.setFastestInterval(5000);



        mLocationCallback2 = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {

                for(final Location location : locationResult.getLocations()){

                    getGeoCoder(location);
                    arrayLatitude.add(location.getLatitude());
                    arrayLongitude.add(location.getLongitude());

                    if(arrayLatitude.size()>1){
                        Location loc1 = new Location("");
                        loc1.setLatitude(arrayLatitude.get(arrayLatitude.size()-1));
                        loc1.setLongitude(arrayLongitude.get(arrayLongitude.size()-1));
                        Location loc2 = new Location("");
                        Double lat2 = arrayLatitude.get(arrayLatitude.size()-2);
                        Double lon2 = arrayLongitude.get(arrayLongitude.size()-2);
                        loc2.setLatitude(lat2);
                        loc2.setLongitude(lon2);
                        double distanceInMeters2 = loc1.distanceTo(loc2);
                        if(distanceInMeters2 > 1){
                            distanceInMeters+=distanceInMeters2;
                        }
                        DecimalFormat df = new DecimalFormat("#.#");
                        df.setRoundingMode(RoundingMode.FLOOR);
                        String distancia = df.format(distanceInMeters);
                        txtDistancia.setText(distancia +"m");


                    }

                }
            }
        };




        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count = 0;

                txtNumPassos.setText("" +count);
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start(); // get base time from a chronometer
                model = ViewModelProviders.of(getActivity()).get(AtividadeViewModel.class);
                String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                Atividade a = new Atividade(0, currentDate, currentTime);
                model.insere(a);
                startLocationUpdates();



            }
        });
        btnParar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                     NotificationCompat.Builder mBuild = createNotification(distanceInMeters,(int) (SystemClock.elapsedRealtime() - chronometer.getBase()));
                    notificationManager.notify(notificationId, mBuild.build());
                    stopLocationUpdates();
                    Atividades = model.getAllAtividades();
                    Atividades.observe(getActivity(), new Observer<List<Atividade>>() {

                        @Override
                        public void onChanged(List<Atividade> atividades) {
                                Atividades.removeObserver(this);
                                Atividade a  = atividades.get(0);
                                final String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                                a.setFim(currentTime);
                                 int duracao = (int) (SystemClock.elapsedRealtime() - chronometer.getBase());
                                a.setDuracao(duracao);
                                a.setDistancia(distanceInMeters);
                                a.setPassos(count);
                                colocarPontos();
                                model.updateAtividade(a);



                                //Atividades.observeForever(this);
                        }
                    });

                    FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    fragmentManager.popBackStack();
                    AddFragment addFragment = new AddFragment();
                    transaction.remove(addFragment);
                    transaction.add(R.id.contentor,addFragment);
                    transaction.commit();

                /*}else{
                    removerPontos();

                    NotificationCompat.Builder mBuild = createNotificationFail();
                    notificationManager.notify(notificationId, mBuild.build());
                    stopLocationUpdates();
                     Atividades = model.getAllAtividades();
                    Atividades.observe(getActivity(), new Observer<List<Atividade>>() {
                        @Override
                        public void onChanged(List<Atividade> atividades) {
                            if(atividades.size() > 0){
                                Atividades.removeObserver(this);
                                Atividade a = atividades.get(0);
                                model.deleteAtividade(a);

                            }else{

                            }

                        }

                    });
                    FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    fragmentManager.popBackStack();
                    AddFragment addFragment = new AddFragment();
                    transaction.remove(addFragment);
                    transaction.add(R.id.contentor,addFragment);
                    transaction.commit();

                }*/

                chronometer.stop();
                txtNumPassos.setText("0");
                txtDistancia.setText("0");
                chronometer.setBase(SystemClock.elapsedRealtime());




            }
        });

        SupportMapFragment m = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragmentoAdd);
        m.getMapAsync(this);

        return  v;
    }


    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess( Location location) {
                if (location != null) {
                    model = ViewModelProviders.of(getActivity()).get(AtividadeViewModel.class);
                    pointsViewModel =   ViewModelProviders.of(getActivity()).get(PointsViewModel.class);
                     model.getAllAtividades().observe(getActivity(), new Observer<List<Atividade>>() {
                        @Override
                        public void onChanged(List<Atividade> atividades) {
                            ultimaAtividade = atividades.get(0);

                        }
                    });

                    Points p = new Points(0,ultimaAtividade.getId(),location.getLatitude(),location.getLongitude());
                    pointsViewModel.insere(p);
                }
            }
        })
                .addOnFailureListener(getActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_FINE_LOCATION);
    }
    private void startLocationUpdatesHOME(){
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;
        }

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,mLocationCallback,null);

    }
    private void startLocationUpdates(){
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;

        }

        mFusedLocationClient2.requestLocationUpdates(mLocationRequest2,mLocationCallback2,null);
        btnStart.setVisibility(View.INVISIBLE);
        btnParar.setVisibility(View.VISIBLE);
        navigationView.setVisibility(View.INVISIBLE);





    }
    private void stopLocationUpdates(){

        mFusedLocationClient2.removeLocationUpdates(mLocationCallback2);



        Toast.makeText(getContext(), "Atividade Terminada ", Toast.LENGTH_LONG).show();
        btnStart.setVisibility(View.VISIBLE);
        btnParar.setVisibility(View.INVISIBLE);
        navigationView.setVisibility(View.VISIBLE);
    }

    public void colocarPontos(){
         AtividadeViewModel model2 ;
        model2 = ViewModelProviders.of(getActivity()).get(AtividadeViewModel.class);
        model2.getAllAtividades().observe(getActivity(), new Observer<List<Atividade>>() {
            @Override
            public void onChanged(List<Atividade> atividades) {
                if(atividades.size()>0){
                    ultimaAtividade = atividades.get(0);
                    while (arrayLatitude.size() > 0) {
                        Points p = new Points(0, ultimaAtividade.getId(), arrayLatitude.get(0), arrayLongitude.get(0));
                        if (pointsViewModel == null) {
                            pointsViewModel = ViewModelProviders.of(getActivity()).get(PointsViewModel.class);
                        }

                        pointsViewModel.insere(p);
                        arrayLongitude.remove(0);
                        arrayLatitude.remove(0);

                    }
                }

            }

        });


    }
    public void removerPontos(){

                while (arrayLatitude.size() > 0) {

                    arrayLongitude.remove(0);
                    arrayLatitude.remove(0);
                }


    }


    public void getGeoCoder(Location location){
        mGeoCoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> lista = mGeoCoder.getFromLocation(location.getLatitude(),location.getLongitude(),1);

            txtGeoCoder.setText(lista.get(0).getAddressLine(0));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;
        startLocationUpdatesHOME();

    }

    private void addCircle(double lat, double longi){
        mGoogleMap.clear();
        LatLng latLng = new LatLng(lat, longi);
         mGoogleMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(10)
                                 .strokeColor(Color.argb(50,0,0,10))
                .fillColor(Color.argb(50,0,0,10)));
        mGoogleMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(2)
                .strokeColor(Color.argb(50,0,0,40))
                .fillColor(Color.argb(50,0,0,40)));
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));

    }


    @Override
    public void onSensorChanged(SensorEvent event) {

            count++;
            txtNumPassos.setText("" +count);


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    public int getPassos(){
        return count;
    }
    private NotificationCompat.Builder createNotificationFail() {


        Intent resulIntent = new Intent(getContext(), AddFragment.class);
        PendingIntent resulPedingIntent = PendingIntent.getActivity(context, notificationId, resulIntent, PendingIntent.FLAG_UPDATE_CURRENT);

//        Intent botao = new Intent(ACTION);
//        PendingIntent boatoPedingIntent = PendingIntent.getBroadcast(this, notificationId, botao, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle("Ups!")
                .setContentText("Isso foi muito pouco. Mova-se no mínimo 10 metros!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(resulPedingIntent);
        //.addAction(R.drawable.notification_icon, "UpdateMe", boatoPedingIntent);

        return mBuilder;
    }
    private NotificationCompat.Builder createNotification(Double teste, int tempo) {
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.FLOOR);
        String distancia = df.format(teste);
        String duracao = String.valueOf(SystemClock.elapsedRealtime() - tempo);

        Intent resulIntent = new Intent(getContext(), AddFragment.class);
        PendingIntent resulPedingIntent = PendingIntent.getActivity(context, notificationId, resulIntent, PendingIntent.FLAG_UPDATE_CURRENT);

//        Intent botao = new Intent(ACTION);
//        PendingIntent boatoPedingIntent = PendingIntent.getBroadcast(this, notificationId, botao, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle("Atividade!")
                .setContentText("Conclui a sua ativida com: " + String.valueOf(distancia) + "m percorridos! " )
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(resulPedingIntent);
        //.addAction(R.drawable.notification_icon, "UpdateMe", boatoPedingIntent);

        return mBuilder;
    }
    private void createNotificationChannel() {
        notificationManager = getActivity().getSystemService(NotificationManager.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "teste";
            String description = "teste2";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);


            notificationManager.createNotificationChannel(channel);
        }
    }



}





