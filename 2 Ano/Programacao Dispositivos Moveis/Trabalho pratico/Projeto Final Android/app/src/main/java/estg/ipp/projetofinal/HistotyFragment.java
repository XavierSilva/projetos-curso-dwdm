package estg.ipp.projetofinal;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import estg.ipp.projetofinal.adapter.AtividadeAdapter;
import estg.ipp.projetofinal.adapter.PointsAdapter;
import estg.ipp.projetofinal.database.entity.Atividade;
import estg.ipp.projetofinal.database.entity.Points;
import estg.ipp.projetofinal.viewModel.AtividadeViewModel;
import estg.ipp.projetofinal.viewModel.PointsViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistotyFragment extends Fragment {

    private AtividadeAdapter adapterAtividade;
    private PointsAdapter adapterPoint;
    private AtividadeViewModel model;
    private PointsViewModel pointsViewModel;
    private Context context;
    private Atividade ultimaAtividade ;

    public HistotyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_histoty_fragmente, container, false);
        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.mRecycler);
        adapterAtividade = new AtividadeAdapter(getContext());
        adapterPoint = new PointsAdapter(getContext());
        recyclerView.setAdapter(adapterAtividade);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        model = ViewModelProviders.of(this).get(AtividadeViewModel.class);
        model.getAllAtividades().observe(this, new Observer<List<Atividade>>() {
            @Override
            public void onChanged(List<Atividade> atividades) {
        adapterAtividade.setAtividades(atividades);
            }
        });
        pointsViewModel = ViewModelProviders.of(this).get(PointsViewModel.class);
        pointsViewModel.getAllPoints().observe(this, new Observer<List<Points>>() {
            @Override
            public void onChanged(List<Points> points) {

         adapterPoint.setPoints(points);
            }
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        this.context = context;
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

}
