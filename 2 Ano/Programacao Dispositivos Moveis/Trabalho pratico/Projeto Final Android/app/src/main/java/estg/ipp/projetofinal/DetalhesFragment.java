package estg.ipp.projetofinal;


import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

import estg.ipp.projetofinal.adapter.AtividadeAdapter;
import estg.ipp.projetofinal.database.entity.Atividade;
import estg.ipp.projetofinal.database.entity.Points;
import estg.ipp.projetofinal.viewModel.AtividadeViewModel;
import estg.ipp.projetofinal.viewModel.PointsViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetalhesFragment extends Fragment implements OnMapReadyCallback  {

    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;
    private Context c;
    private Fragment childFragment;
    TextView txtID;
    PointsViewModel pointsViewModel;
    LiveData<List<Points>>  Pontos;
    LiveData<List<Atividade>>  Atividades;
    List<Points> cordenadas = null;
    double distanceInMeters = 0;
    Button btnDelete;
    private Context context;
    AtividadeViewModel atividadeViewModel;
    public PolylineOptions polylineOptions = new PolylineOptions();
    Chronometer chronometer;
    TextView txtPassos;
    TextView txtDist;

    public DetalhesFragment() {
    }


    @Override
    public void onAttachFragment(Fragment childFragment) {
        c = getActivity();
        this.childFragment = childFragment;
        super.onAttachFragment(childFragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final int idAtividade = getArguments().getInt("idAtividade");
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detalhes, container, false);
        txtID = v.findViewById(R.id.txtId);
        txtID .setText("");
        chronometer = v.findViewById(R.id.simpleChronometerDetalhes);
        chronometer.setBase(SystemClock.elapsedRealtime() -1000);
        btnDelete = v.findViewById(R.id.btnDelete);
        txtPassos = v.findViewById(R.id.txtNumPassosD);
        txtDist = v.findViewById(R.id.txtDistanciaD);
        final AtividadeAdapter atividadeAdapter;
        atividadeViewModel = ViewModelProviders.of(this).get(AtividadeViewModel.class);
        pointsViewModel = ViewModelProviders.of(this).get(PointsViewModel.class);
        Pontos = null;
        Pontos = pointsViewModel.getPointsByID(idAtividade);
        Pontos.observe(this, new Observer<List<Points>>() {
            @Override
            public void onChanged(List<Points> points) {
               cordenadas = points;

               mGoogleMap.clear ();
               for(int i = 0; i<= cordenadas.size()-1;i++){
                   Points p = cordenadas.get(i);

                   double lat = p.getLatitude();
                   double longi = p.getLongitude();
                   if(i == 0){
                       addMarker(lat,longi,0);
                   }else if(i == cordenadas.size()-1){
                       addMarker(lat,longi,1);
                   }else{
                       addMarker(lat,longi,2);
                   }
                   if(i>0){
                       Location loc1 = new
                        Location("");
                       loc1.setLatitude(lat);
                       loc1.setLongitude(longi);
                       Location loc2 = new Location("");
                       Points p2 = cordenadas.get(i-1);
                       Double lat2 = p2.getLatitude();
                       Double lon2 = p2.getLongitude();
                               loc2.setLatitude(lat2);
                       loc2.setLongitude(lon2);
                       distanceInMeters += loc1.distanceTo(loc2);
                       DecimalFormat df = new DecimalFormat("#.#");
                       df.setRoundingMode(RoundingMode.FLOOR);
                       String distancia = df.format(distanceInMeters);
                       txtID.setText("");


                   }


               }
            }
        });
        atividadeViewModel = ViewModelProviders.of(getActivity()).get(AtividadeViewModel.class);

        Atividades = atividadeViewModel.getAtividade(idAtividade);
        Atividades.observe(getActivity(), new Observer<List<Atividade>>() {
            @Override
            public void onChanged(List<Atividade> atividades) {
                if (atividades.size() > 0) {

                    Atividade a = atividades.get(0);
                    txtPassos.setText(String.valueOf(a.getPassos()));

                    DecimalFormat df = new DecimalFormat("#.#");
                    df.setRoundingMode(RoundingMode.FLOOR);
                    String distancia = df.format(a.getDistancia());
                    txtDist.setText(distancia +"m");

                    int duracao = (int) (SystemClock.elapsedRealtime() - a.getDuracao());

                    chronometer.setBase(duracao);


                }



            }});



        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Eliminar");
                alert.setMessage("Tem a certeza que pretende eliminar a Atividade?");
                alert.setPositiveButton("Sim", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int idAtividade = getArguments().getInt("idAtividade");
                        atividadeViewModel = ViewModelProviders.of(getActivity()).get(AtividadeViewModel.class);

                        Atividades = atividadeViewModel.getAtividade(idAtividade);
                        Atividades.observe(getActivity(), new Observer<List<Atividade>>() {
                            @Override
                            public void onChanged(List<Atividade> atividades) {
                                    Atividades.removeObserver(this);
                                    Atividade a = atividades.get(0);
                                    atividadeViewModel.deleteAtividade(a);

                            }});
                        FragmentManager fragmentManager = ((FragmentActivity) getContext()).getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        HistotyFragment histotyFragment = new HistotyFragment();
                        transaction.replace(R.id.contentor, histotyFragment);
                        transaction.commit();
                    }
                });

                alert.setNegativeButton("Não", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alert.show();

            }
        });
        SupportMapFragment m = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragmento);
        m.getMapAsync(this);

        return v;
    }

    @Override
    public void onAttach(Context context) {

            this.context = context;
        super.onAttach(context);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.clear();


    }

    private void addMarker(double lat, double longi, int ref) {
        LatLng latLng = new LatLng(lat, longi);
        if(ref == 0){
             mGoogleMap.addMarker((new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                    .title("Inico")));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 20));
        }else if (ref == 1){
            mGoogleMap.addMarker((new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                    .title("Fim")));

        }
        polylineOptions.add(latLng);
        mGoogleMap.addPolyline(polylineOptions);

    }


}
