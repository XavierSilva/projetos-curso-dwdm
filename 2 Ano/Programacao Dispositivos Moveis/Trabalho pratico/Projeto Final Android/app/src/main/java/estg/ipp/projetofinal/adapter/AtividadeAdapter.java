package estg.ipp.projetofinal.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import estg.ipp.projetofinal.DetalhesFragment;
import estg.ipp.projetofinal.HistotyFragment;
import estg.ipp.projetofinal.R;
import estg.ipp.projetofinal.SecondActivity;
import estg.ipp.projetofinal.database.entity.Atividade;
import estg.ipp.projetofinal.database.entity.Points;

import static android.content.ContentValues.TAG;

public class AtividadeAdapter extends RecyclerView.Adapter<AtividadeAdapter.AtividadeHolder>  {
    private List<Atividade> atividades;
    int position;
    private  Context context;


    public AtividadeAdapter(Context context) {

    }

    public void setAtividades(List<Atividade> atividades){
        this.atividades = atividades;
        notifyDataSetChanged();
    }



    @NonNull
    @Override
    public AtividadeAdapter.AtividadeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.layout_list_atividades, parent, false);
        context = parent.getContext();

        return new AtividadeHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AtividadeAdapter.AtividadeHolder holder, int position) {
        if(atividades != null){
             final Atividade atividade = atividades.get(position);

            if(holder != null){
                String data = atividade.getData();
                String inicio = atividade.getInicio();
                String fim = atividade.getFim();
                holder.txtData.setText("Data: " + data);
                holder.txtInicio.setText("Inicio: " + inicio);
                holder.txtFim.setText("Fim: " + fim);

                holder.btnDetalhes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        DetalhesFragment detalhesFragment = new DetalhesFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt("idAtividade",atividade.getId());
                        detalhesFragment.setArguments(bundle);
                        transaction.replace(R.id.contentor, detalhesFragment);
                        transaction.commit();

                    }

                });
            }

        }
    }

    @Override
    public int getItemCount() {
        if (atividades != null)
            return atividades.size();
        return 0;
    }




    public class AtividadeHolder extends RecyclerView.ViewHolder {
        TextView txtData;
        TextView txtInicio;
        TextView txtFim;
        Button btnDetalhes;

        public AtividadeHolder(@NonNull View itemView) {
            super(itemView);
            btnDetalhes = itemView.findViewById(R.id.btnDetalhes);
            txtData = itemView.findViewById(R.id.txtData);
            txtInicio = itemView.findViewById(R.id.txtInicio);
            txtFim = itemView.findViewById(R.id.txtFim);
        }
    }
}
