package estg.ipp.projetofinal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import android.content.DialogInterface;
import android.location.Address;
import android.os.Bundle;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import estg.ipp.projetofinal.database.entity.Atividade;
import estg.ipp.projetofinal.viewModel.AtividadeViewModel;

public class SecondActivity extends AppCompatActivity implements Cliques {
    private boolean isSmartPhone;
    public BottomNavigationView navigationView;
    private AtividadeViewModel model;

    public BottomNavigationView getNavigationView(){
        return this.navigationView;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        navigationView = (BottomNavigationView) findViewById(R.id.navigationView);
        navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        model = ViewModelProviders.of(this).get(AtividadeViewModel.class);

        if (findViewById(R.id.contentor) != null) {
            isSmartPhone = true;
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            HomeFragment homeFragment = new HomeFragment();
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.contentor, homeFragment);
            transaction.commit();
        } else {
            isSmartPhone = false;
        }

        if (isSmartPhone) {
            /*
            AddFragment homeFragment = new AddFragment();
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();

            transaction.add(R.id.contentor, homeFragment);
            transaction.commit();

             */
        }
    }

/*    @Override
    public void mudarFrag() {
        ListarAtividadesFragment listarFragment = new ListarAtividadesFragment();

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        transaction.replace(R.id.contentor, listarFragment);
        transaction.addToBackStack(null);

        transaction.commit();
    }

    @Override
    public void sendMensage(int id, String data, String inicio) {
        Atividade atividade = new Atividade(id,data,inicio);
        model.insere(atividade);
    }*/


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.home:
                    HomeFragment homeFragment = new HomeFragment();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.contentor, homeFragment);
                    transaction.commit();
                    return true;
                case R.id.add:
                    AddFragment addFragment = new AddFragment();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.contentor, addFragment).commit();
                    return true;
                case R.id.history:
                    HistotyFragment histotyFragment = new HistotyFragment();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.contentor, histotyFragment).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)

                .setTitle("Confirmar")
                .setMessage("Quer mesmo sair da aplicação?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onDestroy();
                        finish();
                    }

                })
                .setNegativeButton("Não", null)
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
